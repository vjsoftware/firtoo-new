<style media="screen">
  .profilepic{
    max-height: 200px;
  }
  .element {
  display: inline-flex;
  align-items: center;
}
i.fa-pencil {
  margin: 0px;
  cursor: pointer;
  font-size: 30px;
  position: absolute;
  left: 0;
  margin-left: 5%;
  margin-top: 10px;
}
i:hover {
  opacity: 0.6;
}
</style>
{{-- <input type="file" name="" id="" style="display:none;"> --}}
<form class="" action="" method="post" enctype="multipart/form-data" id="FileUpload">
  <input type="hidden" name="cropped_value" id="cropped_value" value="" placeholder="hhh">
  <input type="file" name="file" id="cropper" style="display:none;"/>
</form>
<div class="tz-l">
  <div class="tz-l-1">
    <ul>
      @if (Auth::user()->image)
        <li>
          @if ($pageId == 83)
            <i class="fa fa-pencil" style="color: black"></i>
            <span class="name"></span>
          @endif
          <img id="profilePicDynamic" src="{{Auth::user()->image}}" alt="" class="profilepic" style="max-height: none;"/>
        </li>
      @else
        <li>
          @if ($pageId == 83)
            <i class="fa fa-pencil" style="color: black"></i>
            <span class="name"></span>
          @endif
          <img id="profilePicDynamic" src="{{config('app.url')}}/images/avatar.jpg" alt="" class="profilepic"/>
        </li>
      @endif
      <li class="black" style="width: 100%; padding: 0px; font-size: 18px;">{{Auth::user()->name}}</li>

    </ul>
  </div>
  <div class="tz-l-2">
    <ul>
      <li>
        <a href="/dashboard" class="tz-lma"><img src="{{config('app.url')}}/images/icon/dbl1.png" alt="" /> My Dashboard</a>
      </li>

      <li>
        <a href="/intel"><img src="{{config('app.url')}}/images/icon/dbl6.png" alt="" /> My Profile</a>
      </li>
      <li>
        <a href="/listing/create"><img src="{{config('app.url')}}/images/icon/dbl3.png" alt="" /> Add New Listing</a>
      </li>
      <li>
        <a href="/listing"><img src="{{config('app.url')}}/images/icon/dbl2.png" alt="" /> MY Listings</a>
      </li>
      {{-- <li>
        <a href="/message"><img src="{{config('app.url')}}/images/icon/dbl14.png" alt="" /> Messages(12)</a>
      </li>
      <li>
        <a href="/review"><img src="{{config('app.url')}}/images/icon/dbl13.png" alt="" /> Reviews(05)</a>
      </li> --}}
      <li>
        <a href="/addposting/create"><img src="{{config('app.url')}}/images/icon/dbl11.png" alt="" />Add Classifieds</a>
      </li>
      <li>
        <a href="/addposting"><img src="{{config('app.url')}}/images/icon/dbl11.png" alt="" />My Classifieds</a>
      </li>
      <li>
        <a href="/user/logout"><img src="{{config('app.url')}}/images/icon/dbl12.png" alt="" /> Log Out</a>
      </li>
    </ul>
  </div>
</div>
<script src="{{config('app.url')}}/js/jquery.min.js"></script>
<script type="text/javascript">
$("i").click(function () {
  // alert('click');
$("input[type='file']").trigger('click');
});

// $('input[type="file"]').on('change', function() {
// var val = $(this).val();
// $(this).siblings('span').text(val);
// })
</script>
