@php
  use App\User;
  use App\Addposting;
  use App\ClassifiedReviews;
  $pageId = 2;
@endphp
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from rn53themes.net/themes/demo/directory/listing-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:45:41 GMT -->
<head>
	<title>Ads Details | Firtoo</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- FAV ICON(BROWSER TAB ICON) -->
	<link rel="shortcut icon" href="{{config('app.url')}}/images/fav.ico" type="image/x-icon">
	<!-- GOOGLE FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
	<!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="{{config('app.url')}}/css/font-awesome.min.css">
	<!-- ALL CSS FILES -->
	<link href="{{config('app.url')}}/css/materialize.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/style.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<link href="{{config('app.url')}}/css/responsive.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="{{config('app.url')}}/js/html5shiv.js"></script>
	<script src="{{config('app.url')}}/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--TOP SEARCH SECTION-->
	@include('frontend.fixedsearchbar')


	<!--LISTING DETAILS-->

	<section class="list-pg-bg">
		<div class="container">
			<div class="row">
				<div class="com-padd">
					<div class="list-pg-lt list-page-com-p">
						<!--LISTING DETAILS: LEFT PART 1-->
						<div class="pglist-p1 pglist-bg pglist-p-com" id="ld-abour">
							<div class="pglist-p-com-ti">

								<h3>{{$list->item}}  <span class="pull-right">AppId:{{$list->id}}</span> </h3>
              </div>
							<div class="list-pg-inn-sp">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
									<!-- Indicators -->
									<ul class="carousel-indicators">
                    @if ($list->image != '')
                      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    @endif
                    @if ($list->image2 != '')
                      <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                    @endif
                    @if ($list->image3 != '')
                      <li data-target="#myCarousel" data-slide-to="2" class=""></li>
                    @endif
									</ul>
									<!-- Wrapper for slides -->
									<div class="carousel-inner">
                      {{-- {{ $gallery }} --}}
                      @if ($list->image != '')
                        <div class="item active">
                          <img src="{{ $list->image }}" alt="Gallery" style="max-height: 400px; width: auto; margin: 0 auto;" alt="Gallery">
                        </div>
                      @endif
                      @if ($list->image2 != '')
                        <div class="item">
                          <img src="{{ $list->image2 }}" alt="Gallery" style="max-height: 400px; width: auto; margin: 0 auto;" alt="Gallery">
                        </div>
                      @endif
                      @if ($list->image3 != '')
                        <div class="item">
                          <img src="{{ $list->image3 }}" alt="Gallery" style="max-height: 400px; width: auto; margin: 0 auto;" alt="Gallery">
                        </div>
                      @endif

										{{-- <div class="item"> <img src="{{config('app.url')}}/images/slider/2.jpg" alt="Chicago"> </div>
										<div class="item"> <img src="{{config('app.url')}}/images/slider/3.jpg" alt="New York"> </div>
										<div class="item"> <img src="{{config('app.url')}}/images/slider/4.jpg" alt="New York"> </div> --}}
                    <a class="left carousel-control" style="background: none;" href="#myCarousel" role="button" data-slide="prev">
                      <i class="fa fa-angle-left fa-6" aria-hidden="true" style="line-height: 400px; font-size: 85px;"></i>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" style="background: none;" href="#myCarousel" role="button" data-slide="next">
                      <i class="fa fa-angle-right fa-6" aria-hidden="true" style="line-height: 400px; font-size: 85px;"></i>
                      <span class="sr-only">Next</span>
                    </a>
									</div>
									<!-- Left and right controls -->
									{{-- <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <i class="fa fa-angle-left list-slider-nav" aria-hidden="true"></i> </a>
									<a class="right carousel-control" href="#myCarousel" data-slide="next"> <i class="fa fa-angle-right list-slider-nav list-slider-nav-rp" aria-hidden="true"></i> </a> --}}
								</div>
                {{-- <img src="{{config('app.url')}}/posts/{{$list->image}}" style="max-width: 500px;" alt="">
                <hr>
                <div class="row">
                  @if ($list->image)
                    <img style="width:10%" src="{{config('app.url')}}/posts/{{$list->image}}" alt="">
                  @endif
                  @if ($list->image2)
                    <img style="width:10%" src="{{config('app.url')}}/posts/{{$list->image}}" alt="">
                  @endif
                  @if ($list->image3)
                    <img style="width:10%" src="{{config('app.url')}}/posts/{{$list->image}}" alt="">
                  @endif
                </div>
                <p>{{$list->description}}</p>
                <div class="share-btn">
                  <ul>
                    <li><a href="#"><i class="fa fa-facebook fb1"></i> Share On Facebook</a> </li>
                    <li><a href="#"><i class="fa fa-twitter tw1"></i> Share On Twitter</a> </li>
                    <li><a href="#"><i class="fa fa-google-plus gp1"></i> Share On Google Plus</a> </li>
                  </ul>
                </div> --}}
							</div>
						</div>
						<!--END LISTING DETAILS: LEFT PART 1-->

						<!--LISTING DETAILS: LEFT PART 3-->
						{{-- <div class="pglist-p3 pglist-bg pglist-p-com" id="ld-gal">
							<div class="pglist-p-com-ti">
								<h3><span>Photo</span> Gallery</h3> </div>
							<div class="list-pg-inn-sp">
								<div id="myCarousel" class="carousel slide" data-ride="carousel">
									<!-- Indicators -->
									<ol class="carousel-indicators">
										<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
										<li data-target="#myCarousel" data-slide-to="1"></li>
										<li data-target="#myCarousel" data-slide-to="2"></li>
										<li data-target="#myCarousel" data-slide-to="3"></li>
									</ol>
									<!-- Wrapper for slides -->
									<div class="carousel-inner">
										<div class="item active"> <img src="{{config('app.url')}}/posts/{{$list->image}}" alt="Los Angeles"> </div>
										<div class="item"> <img src="{{config('app.url')}}/images/slider/2.jpg" alt="Chicago"> </div>
										<div class="item"> <img src="{{config('app.url')}}/images/slider/3.jpg" alt="New York"> </div>
										<div class="item"> <img src="{{config('app.url')}}/images/slider/4.jpg" alt="New York"> </div>
									</div>
									<!-- Left and right controls -->
									<a class="left carousel-control" href="#myCarousel" data-slide="prev"> <i class="fa fa-angle-left list-slider-nav" aria-hidden="true"></i> </a>
									<a class="right carousel-control" href="#myCarousel" data-slide="next"> <i class="fa fa-angle-right list-slider-nav list-slider-nav-rp" aria-hidden="true"></i> </a>
								</div>
							</div>
						</div> --}}
						<!--END LISTING DETAILS: LEFT PART 3-->
						<!--LISTING DETAILS: LEFT PART 4-->
						<div class="pglist-p3 pglist-bg pglist-p-com" id="ld-roo">
							<div class="pglist-p-com-ti">
								<h3><span>Classified</span> Description</h3> </div>
							<div class="list-pg-inn-sp">
                <p>{{ $list->description }}</p>
							</div>
						</div>
						<div class="pglist-p3 pglist-bg pglist-p-com" id="ld-roo">
							<div class="pglist-p-com-ti">
								<h3><span>Similar</span> Classifieds</h3> </div>
							<div class="list-pg-inn-sp">
                @php

                $all_postings = Addposting::where('category_id',$list->category_id)->where('id', '!=', $list->id)->where('status', 4)->get();
                @endphp
								@foreach ($all_postings as $value)
                  <div class="home-list-pop list-spac list-spac-1 list-room-mar-o">
  									<!--LISTINGS IMAGE-->
                    @if ($value->image)
                      <div class="col-md-3"> <img src="{{$value->image}}" alt=""> </div>
                      @else
                        <div class="col-md-3"> <img src="{{config('app.url')}}/images/room/1.jpg" alt=""> </div>
                    @endif
  									<!--LISTINGS: CONTENT-->
  									<div class="col-md-9 home-list-pop-desc inn-list-pop-desc list-room-deta"> <a href="/classifieds/{{$value->id}}"><h3>{{$value->item}}</h3></a>
  										<div class="list-rat-ch list-room-rati"> <span>5.0</span> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> </div>
  										{{-- <div class="list-room-type list-rom-ami">
  											<ul>
  												<li>
  													<p><b>Amenities:{{$value->id}}</b> </p>
  												</li>
  												<li><img src="{{config('app.url')}}/images/icon/a7.png" alt=""> Wi-Fi</li>
  												<li><img src="{{config('app.url')}}/images/icon/a4.png" alt=""> Air Conditioner </li>
  												<li><img src="{{config('app.url')}}/images/icon/a3.png" alt=""> Swimming Pool</li>
  												<li><img src="{{config('app.url')}}/images/icon/a2.png" alt=""> Bar</li>
  												<li><img src="{{config('app.url')}}/images/icon/a5.png" alt=""> Bathroom</li>
  												<li><img src="{{config('app.url')}}/images/icon/a6.png" alt=""> TV</li>
  												<li><img src="{{config('app.url')}}/images/icon/a9.png" alt=""> Spa</li>
  												<li><img src="{{config('app.url')}}/images/icon/a10.png" alt=""> Music</li>
  												<li><img src="{{config('app.url')}}/images/icon/a11.png" alt=""> Parking</li>
  											</ul>
  										</div> --}}
                       <span class="home-list-pop-rat list-rom-pric">Rs.{{$value->price}}</span>
  										<div class="list-enqu-btn">
  											<ul>
  												<li><a href="#!"><i class="fa fa-usd" aria-hidden="true"></i> Get Quotes</a> </li>
  												<li><a href="#!"><i class="fa fa-commenting-o" aria-hidden="true"></i> Send SMS</a> </li>
  												<li><a href="#!"><i class="fa fa-phone" aria-hidden="true"></i> Call Now</a> </li>
  												<li><a href="#!"><i class="fa fa-usd" aria-hidden="true"></i> Book Now</a> </li>
  											</ul>
  										</div>
  									</div>
  								</div>

                @endforeach

							</div>
						</div>
            <!--LISTING DETAILS: LEFT PART 6-->
            <div class="pglist-p3 pglist-bg pglist-p-com" id="ld-rew">
              <div class="pglist-p-com-ti">
                <h3><span>Write Your</span> Reviews</h3> </div>
              <div class="list-pg-inn-sp">
                <div class="list-pg-write-rev">
                    <form class="col" action="{{action('ClassifiedReviewsController@store')}}" method="post">
                       {{ csrf_field() }}
                    <p>Writing great reviews may help others that are just apt for them. Here are a few tips to write a good review:</p>
                    {{-- <div class="row">
                      <div class="col s12">
                        <fieldset class="rating">
                          <input type="radio" id="star5" name="rating" value="5" />
                          <label class="full" for="star5" title="Awesome - 5 stars"></label>
                          <input type="radio" id="star4half" name="rating" value="4 and a half" />
                          <label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                          <input type="radio" id="star4" name="rating" value="4" />
                          <label class="full" for="star4" title="Pretty good - 4 stars"></label>
                          <input type="radio" id="star3half" name="rating" value="3 and a half" />
                          <label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                          <input type="radio" id="star3" name="rating" value="3" />
                          <label class="full" for="star3" title="Meh - 3 stars"></label>
                          <input type="radio" id="star2half" name="rating" value="2 and a half" />
                          <label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                          <input type="radio" id="star2" name="rating" value="2" />
                          <label class="full" for="star2" title="Kinda bad - 2 stars"></label>
                          <input type="radio" id="star1half" name="rating" value="1 and a half" />
                          <label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                          <input type="radio" id="star1" name="rating" value="1" />
                          <label class="full" for="star1" title="Sucks big time - 1 star"></label>
                          <input type="radio" id="starhalf" name="rating" value="half" />
                          <label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                        </fieldset>
                      </div>
                    </div> --}}
                    {{-- <div class="row">
                      <div class="input-field col s6">
                        <input id="re_name" type="text" class="validate">
                        <label for="re_name">Full Name</label>
                      </div>
                      <div class="input-field col s6">
                        <input id="re_mob" type="number" class="validate">
                        <label for="re_mob">Mobile</label>
                      </div>

                    </div>
                    <div class="row">
                      <div class="input-field col s6">
                        <input id="re_mail" type="email" class="validate">
                        <label for="re_mail">Email id</label>
                      </div>
                      <div class="input-field col s6">
                        <input id="re_city" type="text" class="validate">
                        <label for="re_city">City</label>
                      </div>
                    </div> --}}
                  @if (Auth::user())
                    <div class="row">
                      <div class="input-field col s12">
                        <textarea id="re_msg" class="materialize-textarea" name="review"></textarea>
                        <input type="hidden" name="classifiedid" value="{{$list->id}}">
                        <label for="re_msg">Write U'r Review</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s12">
                        <input type="submit" value="Submit Review" class="waves-effect waves-light btn-large full-btn" />
                      </div>
                    </div>
                  </form>
                    @else

                      <div class="row">
                        <div class="input-field col s12">
                          <a href="/login" class="waves-effect waves-light btn-large full-btn">Please Login to Write a Review</a>
                          {{-- <input type="submit" value="Please Login to Write a Review" class="waves-effect waves-light btn-large full-btn" /> --}}
                        </div>
                      </div>
                  @endif

                </div>
              </div>
            </div>
            <!--END LISTING DETAILS: LEFT PART 6-->
            <!--LISTING DETAILS: LEFT PART 5-->
            <div class="pglist-p3 pglist-bg pglist-p-com" id="ld-rer">
              <div class="pglist-p-com-ti">
                <h3><span>User</span> Reviews</h3> </div>
              <div class="list-pg-inn-sp">
                {{-- <div class="lp-ur-all">
                  <div class="lp-ur-all-left">
                    <div class="lp-ur-all-left-1">
                      <div class="lp-ur-all-left-11">Excellent</div>
                      <div class="lp-ur-all-left-12">
                        <div class="lp-ur-all-left-13"></div>
                      </div>
                    </div>
                    <div class="lp-ur-all-left-1">
                      <div class="lp-ur-all-left-11">Good</div>
                      <div class="lp-ur-all-left-12">
                        <div class="lp-ur-all-left-13 lp-ur-all-left-Good"></div>
                      </div>
                    </div>
                    <div class="lp-ur-all-left-1">
                      <div class="lp-ur-all-left-11">Satisfactory</div>
                      <div class="lp-ur-all-left-12">
                        <div class="lp-ur-all-left-13 lp-ur-all-left-satis"></div>
                      </div>
                    </div>
                    <div class="lp-ur-all-left-1">
                      <div class="lp-ur-all-left-11">Below Average</div>
                      <div class="lp-ur-all-left-12">
                        <div class="lp-ur-all-left-13 lp-ur-all-left-below"></div>
                      </div>
                    </div>
                    <div class="lp-ur-all-left-1">
                      <div class="lp-ur-all-left-11">Below Average</div>
                      <div class="lp-ur-all-left-12">
                        <div class="lp-ur-all-left-13 lp-ur-all-left-poor"></div>
                      </div>
                    </div>
                  </div>
                  <div class="lp-ur-all-right">
                    <h5>Overall Ratings</h5>
                    <p><span>4.5 <i class="fa fa-star" aria-hidden="true"></i></span> based on 242 reviews</p>
                  </div>
                </div> --}}
                <div class="lp-ur-all-rat">
                  {{-- <h5>Reviews</h5> --}}
                  <ul>
                    @php
                      $reviews = ClassifiedReviews::Where('classifiedid', $list->id)->get();
                      // $reviews = ClassifiedReviews::Where('classifiedid', $list->id);
                    @endphp
                    @foreach ($reviews as $allreviews)
                      <li>
                        <div class="lr-user-wr-img">
                          {{-- <img src="images/users/2.png" alt=""> --}}
                         </div>
                        <div class="lr-user-wr-con">
                          @php
                            $finduser = User::Where('id', $allreviews->userid);
                            $username = $finduser->first()->name;
                          @endphp
                          <h6>{{$username}}
                            {{-- <span>4.5 <i class="fa fa-star" aria-hidden="true"></i></span> --}}
                          </h6> <span class="lr-revi-date">{{$allreviews->created_at}}</span>
                          <p>{{$allreviews->review}} </p>
                          {{-- <ul>
                            <li><a href="#!"><span>Like</span><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a> </li>
                            <li><a href="#!"><span>Dis-Like</span><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></a> </li>
                            <li><a href="#!"><span>Report</span> <i class="fa fa-flag-o" aria-hidden="true"></i></a> </li>
                            <li><a href="#!"><span>Comments</span> <i class="fa fa-commenting-o" aria-hidden="true"></i></a> </li>
                            <li><a href="#!"><span>Share Now</span>  <i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                            <li><a href="#!"><i class="fa fa-google-plus" aria-hidden="true"></i></a> </li>
                            <li><a href="#!"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                            <li><a href="#!"><i class="fa fa-linkedin" aria-hidden="true"></i></a> </li>
                            <li><a href="#!"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                          </ul> --}}
                        </div>
                      </li>
                    @endforeach
                  </ul>
                </div>
              </div>
            </div>
            <!--END LISTING DETAILS: LEFT PART 5-->

					</div>
					<div class="list-pg-rt">
						<!--LISTING DETAILS: LEFT PART 7-->
						<div class="pglist-p3 pglist-bg pglist-p-com">
							<div class="pglist-p-com-ti pglist-p-com-ti-right">
								<h3><span>Pricing</span> details</h3> </div>
							<div class="list-pg-inn-sp">
								<div class="list-pg-guar">

											<div class="list-pg-guar-img"> <img src="{{config('app.url')}}/images/icon/g1.png" alt="" /> </div>
											<h3>Price: {{$list->price}}</h3>

                </div>
							</div>
						</div>
						<!--END LISTING DETAILS: LEFT PART 7-->
						<!--LISTING DETAILS: LEFT PART 7-->
            @php
              $user = User::Where('id', $list->user_id);
            @endphp
						<div class="pglist-p3 pglist-bg pglist-p-com">
							{{-- <div class="pg-list-user-pro"> <img src="{{config('app.url')}}/images/users/8.png" alt=""> </div> --}}
              <div class="center"> <img style="width: 75%;" src="{{$user->first()->image}}" alt=""> </div>
							<div class="list-pg-inn-sp">
								<div class="list-pg-upro">
									<h5>{{$user->first()->name}}</h5>
									<p>Member since July 2017</p> <a class="waves-effect waves-light btn-large full-btn list-pg-btn" href="#!">Contact User</a> </div>
							</div>
						</div>
            <div class="pglist-p3 pglist-bg pglist-p-com">
							<div class="pglist-p-com-ti pglist-p-com-ti-right">
								<h3><span>Contact</span> details</h3> </div>
							<div class="list-pg-inn-sp">
								<div class="list-pg-guar">


											<h3>Mobile No: {{$user->first()->mobile}}</h3>
                      <a class="waves-effect waves-light btn-large full-btn list-pg-btn" href="tel:{{$user->first()->mobile}}">Contact User</a> </div>
                </div>
							</div>
						</div>
						<!--END LISTING DETAILS: LEFT PART 7-->
						<!--LISTING DETAILS: LEFT PART 8-->
						{{-- <div class="pglist-p3 pglist-bg pglist-p-com">
							<div class="pglist-p-com-ti pglist-p-com-ti-right">
								<h3><span>Our</span> Location</h3> </div>
							<div class="list-pg-inn-sp">
								<div class="list-pg-map">
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6290413.804893654!2d-93.99620524741552!3d39.66116578737809!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880b2d386f6e2619%3A0x7f15825064115956!2sIllinois%2C+USA!5e0!3m2!1sen!2sin!4v1469954001005" allowfullscreen></iframe>
								</div>
							</div>
						</div> --}}
						<!--END LISTING DETAILS: LEFT PART 8-->

					</div>
				</div>
			</div>
		</div>
	</section>
	<!--MOBILE APP-->
	{{-- <section class="web-app com-padd">
		<div class="container">
			<div class="row">
				<div class="col-md-6 web-app-img"> <img src="{{config('app.url')}}/images/mobile.png" alt="" /> </div>
				<div class="col-md-6 web-app-con">
					<h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
					<ul>
						<li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
					</ul> <span>We'll send you a link, open it on your phone to download the app</span>
					<form>
						<ul>
							<li>
								<input type="text" placeholder="+01" /> </li>
							<li>
								<input type="number" placeholder="Enter mobile number" /> </li>
							<li>
								<input type="submit" value="Get App Link" /> </li>
						</ul>
					</form>
					<a href="#"><img src="{{config('app.url')}}/images/android.png" alt="" /> </a>
					<a href="#"><img src="{{config('app.url')}}/images/apple.png" alt="" /> </a>
				</div>
			</div>
		</div>
	</section> --}}
	<!--FOOTER SECTION-->
	@include('frontend.footer')
	<!--QUOTS POPUP-->
	<section>
		<!-- GET QUOTES POPUP -->
		<div class="modal fade dir-pop-com" id="list-quo" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header dir-pop-head">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Get a Quotes</h4>
						<!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
					</div>
					<div class="modal-body dir-pop-body">
						<form method="post" class="form-horizontal">
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Full Name *</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="fname" placeholder="" required> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Mobile</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="mobile" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Email</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="email" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Message</label>
								<div class="col-md-8 get-quo">
									<textarea class="form-control"></textarea>
								</div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<div class="col-md-6 col-md-offset-4">
									<input type="submit" value="SUBMIT" class="pop-btn"> </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- GET QUOTES Popup END -->
	</section>
	<!--SCRIPT FILES-->
	<script src="{{config('app.url')}}/js/jquery.min.js"></script>
	<script src="{{config('app.url')}}/js/bootstrap.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/materialize.min.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/custom.js"></script>
</body>


<!-- Mirrored from rn53themes.net/themes/demo/directory/listing-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:46:14 GMT -->
</html>
