@php
  use App\Category;
  $pageId = 2;
@endphp
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from rn53themes.net/themes/demo/directory/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:44:56 GMT -->
<head>
	<title>Show All Ads | Firtoo</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- FAV ICON(BROWSER TAB ICON) -->
	<link rel="shortcut icon" href="{{config('app.url')}}/images/fav.ico" type="image/x-icon">
	<!-- GOOGLE FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
	<!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="{{config('app.url')}}/css/font-awesome.min.css">
	<!-- ALL CSS FILES -->
	<link href="{{config('app.url')}}/css/materialize.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/style.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<link href="{{config('app.url')}}/css/responsive.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="{{config('app.url')}}/js/html5shiv.js"></script>
	<script src="{{config('app.url')}}/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--TOP SEARCH SECTION-->
	@include('frontend.fixedsearchbar')
	<!--DASHBOARD-->
  <section class="dir-pa-sp-top">
		<div class="container dir-hom-pre-tit">
			<div class="row com-padd com-padd-redu-bot1">
				<div class="com-title">
					<h2>Top Classifieds in <span>this month</span></h2>
					<p>Explore some of the best tips from around the world from our partners and friends.</p>
				</div>
				@foreach ($list as $key)

            <div class="col-md-6">
    					<div>
    						<!--POPULAR LISTINGS-->
    						<div class="home-list-pop">
    							<!--POPULAR LISTINGS IMAGE-->
                  @php
                  $Category = Category::where('id', $key->category_id)->get();
                  @endphp
    							<div class="col-md-3">
                    <a href="/classifieds/{{$key->id}}">
                      <img src="{{$key->image}}" alt=""> 
                    </a>
                  </div>
    							<!--POPULAR LISTINGS: CONTENT-->
    							<div class="col-md-9 home-list-pop-desc"> <a href="/classifieds/{{$key->id}}"><h3>{{$key->item}} || {{ $Category->first()->name}}</h3></a>
    								{{-- <div class="pg-revi-re"><img src="images/users/8.png" alt="">
    									<p>Melanie <span>252 Reviews , 909 Followers</span>
    										<p>
    											<div class="list-rat-ch list-room-rati pg-re-rat"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> </div>
    								</div> --}}
    								<p>{{substr($key->description, 0, 20)}}</p>
                    {{-- <span class="home-list-pop-rat">4.2</span> --}}
    								<div class="hom-list-share">
    									<ul>
    										<li><a href="#"><i class="fa fa-bar-chart" aria-hidden="true"></i> 52</a> </li>
    										<li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i> 32</a> </li>
    										<li><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 420</a> </li>
    										<li><a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i> 570</a> </li>
    									</ul>
    								</div>
    							</div>
    						</div>

    					</div>
    				</div>
        @endforeach

			</div>
		</div>
	</section>
	<!--END DASHBOARD-->
	<!--MOBILE APP-->
	<section class="web-app com-padd">
		<div class="container">
			<div class="row">
				<div class="col-md-6 web-app-img"> <img src="{{config('app.url')}}/images/mobile.png" alt="" /> </div>
				<div class="col-md-6 web-app-con">
					<h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
					<ul>
						<li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
					</ul> <span>We'll send you a link, open it on your phone to download the app</span>
					<form>
						<ul>
							<li>
								<input type="text" placeholder="+01" /> </li>
							<li>
								<input type="number" placeholder="Enter mobile number" /> </li>
							<li>
								<input type="submit" value="Get App Link" /> </li>
						</ul>
					</form>
					<a href="#"><img src="{{config('app.url')}}/images/android.png" alt="" /> </a>
					<a href="#"><img src="{{config('app.url')}}/images/apple.png" alt="" /> </a>
				</div>
			</div>
		</div>
	</section>
	<!--FOOTER SECTION-->
		@include('frontend.footer')
	<!--QUOTS POPUP-->
	<section>
		<!-- GET QUOTES POPUP -->
		<div class="modal fade dir-pop-com" id="list-quo" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header dir-pop-head">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Get a Quotes</h4>
						<!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
					</div>
					<div class="modal-body dir-pop-body">
						<form method="post" class="form-horizontal">
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Full Name *</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="fname" placeholder="" required> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Mobile</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="mobile" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Email</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="email" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Message</label>
								<div class="col-md-8 get-quo">
									<textarea class="form-control"></textarea>
								</div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<div class="col-md-6 col-md-offset-4">
									<input type="submit" value="SUBMIT" class="pop-btn"> </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- GET QUOTES Popup END -->
	</section>
	<!--SCRIPT FILES-->
	<script src="{{config('app.url')}}/js/jquery.min.js"></script>
	<script src="{{config('app.url')}}/js/bootstrap.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/materialize.min.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/custom.js"></script>
</body>


<!-- Mirrored from rn53themes.net/themes/demo/directory/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:45:06 GMT -->
</html>
