<div class="sb2-1">
  <!--== USER INFO ==-->
  <div class="sb2-12">
    <ul>
      <li><img src="{{config('app.url')}}/images/users/2.png" alt=""> </li>
      <li>
        @if (Auth::user())
          <h5>{{Auth::user()->name}}</h5>
        @endif
       </li>
      <li></li>
    </ul>
  </div>
  <!--== LEFT MENU ==-->
  <div class="sb2-13">
    <ul class="collapsible" data-collapsible="accordion">
      <li><a href="/luckylu" class="menu-active"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a> </li>
      <li><a href="javascript:void(0)" class="collapsible-header"><i class="fa fa-list-ul" aria-hidden="true"></i> Listing</a>
        <div class="collapsible-body left-sub-menu">
          <ul>
            <li><a href="/luckylu/reports/listing">All listing</a> </li>
            {{-- <li><a href="admin-list-add.html">Add New listing</a> </li>
            <li><a href="admin-listing-category.html">All listing Categories</a> </li>
            <li><a href="admin-list-category-add.html">Add listing Categories</a> </li> --}}
          </ul>
        </div>
      </li>
      <li><a href="javascript:void(0)" class="collapsible-header"><i class="fa fa-user" aria-hidden="true"></i> Users</a>
        <div class="collapsible-body left-sub-menu">
          <ul>
            <li><a href="/luckylu/reports/users">All Users</a> </li>
          </ul>
        </div>
      </li>
      <li><a href="javascript:void(0)" class="collapsible-header"><i class="fa fa-user" aria-hidden="true"></i> Classifieds</a>
        <div class="collapsible-body left-sub-menu">
          <ul>
            <li><a href="/luckylu/reports/classifieds">All Classifieds</a> </li>
          </ul>
        </div>
      </li>
      <li><a href="javascript:void(0)" class="collapsible-header">
        <i class="fa fa-user" aria-hidden="true"></i> Categories</a>
        <div class="collapsible-body left-sub-menu">
          <ul>
            <li><a href="/luckylu/categories/create">Category</a> </li>
            <li><a href="/luckylu/categories">All Categories</a> </li>
          </ul>
        </div>
      </li>
      <li><a href="javascript:void(0)" class="collapsible-header">
        <i class="fa fa-user" aria-hidden="true"></i> Sub-Categories</a>
        <div class="collapsible-body left-sub-menu">
          <ul>
            <li><a href="/luckylu/subcategories/create">Add SubCategory</a> </li>
            <li><a href="/luckylu/subcategories">All SubCategories</a> </li>
          </ul>
        </div>
      </li>
      <li><a href="javascript:void(0)" class="collapsible-header"><i class="fa fa-buysellads" aria-hidden="true"></i>States</a>
        <div class="collapsible-body left-sub-menu">
          <ul>
            <li><a href="/luckylu/states/create">Add States</a> </li>
            <li><a href="/luckylu/states">All States</a> </li>
          </ul>
        </div>
      </li>
      <li><a href="javascript:void(0)" class="collapsible-header"><i class="fa fa-buysellads" aria-hidden="true"></i>Cities</a>
        <div class="collapsible-body left-sub-menu">
          <ul>
            <li><a href="/luckylu/city/create">Add City</a> </li>
            <li><a href="/luckylu/city">All Cities</a> </li>
          </ul>
        </div>
      </li>
    {{--  <li><a href="admin-analytics.html"><i class="fa fa-bar-chart" aria-hidden="true"></i> Analytics</a> </li>

       <li><a href="admin-payment.html"><i class="fa fa-usd" aria-hidden="true"></i> Payments</a> </li>
      <li><a href="admin-earnings.html"><i class="fa fa-money" aria-hidden="true"></i> Earnings</a> </li>
      <li><a href="javascript:void(0)" class="collapsible-header"><i class="fa fa-bell-o" aria-hidden="true"></i>Notifications</a>
        <div class="collapsible-body left-sub-menu">
          <ul>
            <li><a href="admin-notifications.html">All Notifications</a> </li>
            <li><a href="admin-notifications-user-add.html">User Notifications</a> </li>
            <li><a href="admin-notifications-push-add.html">Push Notifications</a> </li>
          </ul>
        </div>
      </li>
      <li><a href="javascript:void(0)" class="collapsible-header"><i class="fa fa-tags" aria-hidden="true"></i> List Price</a>
        <div class="collapsible-body left-sub-menu">
          <ul>
            <li><a href="admin-price.html">All List Price</a> </li>
            <li><a href="admin-price-list.html">Add New Price</a> </li>
          </ul>
        </div>
      </li>
      <li><a href="javascript:void(0)" class="collapsible-header"><i class="fa fa-rss" aria-hidden="true"></i> Blog & Articals</a>
        <div class="collapsible-body left-sub-menu">
          <ul>
            <li><a href="admin-blog.html">All Blogs</a> </li>
            <li><a href="admin-blog-add.html">Add Blog</a> </li>
          </ul>
        </div>
      </li> --}}
      {{-- <li><a href="admin-setting.html"><i class="fa fa-cogs" aria-hidden="true"></i> Setting</a> </li> --}}
      {{-- <li><a href="admin-social-media.html"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Social Media</a> </li> --}}
      {{-- <li><a href="#" target="_blank"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a> </li> --}}
    </ul>
  </div>
</div>
