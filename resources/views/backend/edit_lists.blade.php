@php
	use App\Category;
	use App\Listing;
	use App\City;
	use App\ListingsGalley;
@endphp
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from rn53themes.net/themes/demo/directory/admin-list-add.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:48:54 GMT -->
<head>
	<title>Edit Listings | Firtoo</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- FAV ICON(BROWSER TAB ICON) -->
	<link rel="shortcut icon" href="{{config('app.url')}}/images/fav.ico" type="image/x-icon">
	<!-- GOOGLE FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
	<!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="{{config('app.url')}}/css/font-awesome.min.css">
	<!-- ALL CSS FILES -->
	<link href="{{config('app.url')}}/css/materialize.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/style.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<link href="{{config('app.url')}}/css/responsive.css" rel="stylesheet">
	<style>
	/* quick reset */
* {
margin: 0;
padding: 0;
border: 0;
}

/* relevant styles */
.img__wrap {
position: relative;
/* height: 200px; */
width: 100px;
}

.img__description {
position: absolute;
top: 0;
bottom: 0;
left: 0;
right: 0;
background: rgba(29, 106, 154, 0.72);
color: #fff;
visibility: hidden;
opacity: 0;
text-align: center;
line-height: 6;

/* transition effect. not necessary */
transition: opacity .2s, visibility .2s;
cursor: pointer;
}

.img__wrap:hover .img__description {
visibility: visible;
opacity: 1;
}
</style>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="{{config('app.url')}}/js/html5shiv.js"></script>
	<script src="{{config('app.url')}}/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--== MAIN CONTRAINER ==-->
	@include('backend.includes.header')
	<!--== BODY CONTNAINER ==-->
	<div class="container-fluid sb2">
		<div class="row">
			@include('backend.includes.sidebar')
			<!--== BODY INNER CONTAINER ==-->
			<div class="sb2-2">
				<!--== breadcrumbs ==-->
				<div class="sb2-2-2">
					<ul>
						<li><a href="/luckylu"><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
						<li class="active-bre"><a href="#"> Edit Lists</a> </li>
						<li class="page-back"><a href="/luckylu"><i class="fa fa-backward" aria-hidden="true"></i> Back</a> </li>
					</ul>
				</div>
				<div class="tz-2 tz-2-admin">
					<div class="tz-2-com tz-2-main">
						<h4>Edit Lists</h4>

						<!-- Dropdown Structure -->
						<div class="split-row">
							<div class="col-md-12">
								<div class="box-inn-sp ad-inn-page">
									<div class="tab-inn ad-tab-inn">
						<div class="hom-cre-acc-left hom-cre-acc-right">
							<div class="">
                {{-- {!! Form::open(['action' => ['AdminReportController@update',$key->id], 'method'=> 'POST']) !!} --}}
								<form class="" action="{{route('updatelists')}}" method="POST">
									{{ csrf_field() }}

										{{-- <input name="_method" type="hidden" value="PUT"> --}}
										<input  type="hidden" name="id" class="validate" value = "{{$key->id}}">
										<input  type="hidden" name="user_id" class="validate" value = "{{$key->user_id}}">

											<div class="input-field col s6">

												<input id="" type="text" name="title" class="validate" value = "{{$key->title}}" placeholder="Title here..">
											</div>
											<div class="col s6">
	                      <div class="input-field">
													<input id="" type="text" name="phone" class="validate" value = "{{$key->phone}}" placeholder="Mobile No">
											</div>
										</div>
										<div class="col s6">
                      <div class="input-field">
												<input id="name" type="email" name="email" class="validate" value = "{{$key->email}}" placeholder="Email">

										</div>
										</div>

										<div class="input-field col s6">
											<select  name="category_id">
												<option value="" disabled selected>Select Category</option>
												@php
													$categories = Category::all();
												@endphp
												@foreach ($categories as $category)

													<option
													@if ($category->id == $key->category_id)
								            selected
								          @endif
													value="{{$category->id}}">{{$category->name}}</option>
												@endforeach
											</select>
										</div>


										<div class="input-field col s6">
											<select  name="city_id">
												<option value="" disabled selected>Select City</option>
												@php
													$cities = City::all();
												@endphp
												@foreach ($cities as $city)

													<option
													@if ($city->id == $key->city_id)
								            selected
								          @endif
													value="{{$city->id}}">{{$city->city}}</option>
												@endforeach
											</select>
										</div>
										<div class="input-field col s6">
											<select multiple name="opening_days">
												<option value="" disabled selected>Opening Days</option>
												<option  @if ($key->opening_days == "8")
													selected
												@endif value="8">All Days</option>
												<option @if ($key->opening_days == "1")
													selected
												@endif value="1">Monday</option>
												<option @if ($key->opening_days == "2")
													selected
												@endif value="2">Tuesday</option>
												<option @if ($key->opening_days == "3")
													selected
												@endif value="3">Wednesday</option>
												<option @if ($key->opening_days == "4")
													selected
												@endif value="4">Thursday</option>
												<option @if ($key->opening_days == "5")
													selected
												@endif value="5">Friday</option>
												<option @if ($key->opening_days == "6")
													selected
												@endif value="6">Saturday</option>
												<option @if ($key->opening_days == "7")
													selected
												@endif value="7">Sunday</option>
											</select>
										</div>

										<div class="input-field col s6">
											<textarea name="address" placeholder="address" >{{$key->address}}</textarea>
											{{-- <input id="name" type="text" name="address" class="validate" value = "{{$key->item}}"> --}}
									</div>

										<div class="input-field col s6">
											<select name="open_time">
												<option value="" disabled selected>Open Time</option>
												<option @if ($key->open_time == "12:00 AM")
													selected
												@endif
												 value="12:00 AM">12:00 AM</option>
												<option @if ($key->open_time == "01:00 AM")
													selected
												@endif value="01:00 AM">01:00 AM</option>
												<option  @if ($key->open_time == "02:00 AM")
													selected
												@endif value="02:00 AM">02:00 AM</option>
												<option   @if ($key->open_time == "03:00 AM")
													selected
												@endif
												value="03:00 AM">03:00 AM</option>
												<option   @if ($key->open_time == "04:00 AM")
													selected
												@endif value="04:00 AM">04:00 AM</option>
												<option  @if ($key->open_time == "05:00 AM")
													selected
												@endif value="05:00 AM">05:00 AM</option>
												<option  @if ($key->open_time == "06:00 AM")
													selected
												@endif value="06:00 AM">06:00 AM</option>
												<option  @if ($key->open_time == "07:00 AM")
													selected
												@endif value="07:00 AM">07:00 AM</option>
												<option   @if ($key->open_time == "08:00 AM")
													selected
												@endif value="08:00 AM">08:00 AM</option>
												<option  @if ($key->open_time == "09:00 AM")
													selected
												@endif value="09:00 AM">09:00 AM</option>
												<option  @if ($key->open_time == "10:00 AM")
													selected
												@endif value="10:00 AM">10:00 AM</option>
												<option  @if ($key->open_time == "11:00 AM")
													selected
												@endif value="11:00 AM">11:00 AM</option>
												<option  @if ($key->open_time == "12:00 AM")
													selected
												@endif value="12:00 PM">12:00 PM</option>
												<option  @if ($key->open_time == "01:00 PM")
													selected
												@endif value="01:00 PM">01:00 PM</option>
												<option @if ($key->open_time == "02:00 PM")
													selected
												@endif value="02:00 PM">02:00 PM</option>
												<option @if ($key->open_time == "03:00 PM")
													selected
												@endif value="03:00 PM">03:00 PM</option>
												<option @if ($key->open_time == "04:00 PM")
													selected
												@endif value="04:00 PM">04:00 PM</option>
												<option @if ($key->open_time == "05:00 PM")
													selected
												@endif value="05:00 PM">05:00 PM</option>
												<option @if ($key->open_time == "06:00 PM")
													selected/luckylu
												@endif value="06:00 PM">06:00 PM</option>
												<option @if ($key->open_time == "07:00 PM")
													selected
												@endif value="07:00 PM">07:00 PM</option>
												<option @if ($key->open_time == "08:00 PM")
													selected
												@endif value="08:00 PM">08:00 PM</option>
												<option @if ($key->open_time == "09:00 PM")
													selected
												@endif value="09:00 PM">09:00 PM</option>
												<option @if ($key->open_time == "10:00 PM")
													selected
												@endif value="10:00 PM">10:00 PM</option>
												<option @if ($key->open_time == "11:00 PM")
													selected
												@endif value="11:00 PM">11:00 PM</option>
											</select>
										</div>

                      <div class="input-field col s6">
												<select name="closing_time">
													<option value="" disabled selected>Closing Time</option>
													<option @if ($key->closing_time == "12:00 AM")
														selected
													@endif
													 value="12:00 AM">12:00 AM</option>
													<option @if ($key->closing_time == "01:00 AM")
														selected
													@endif value="01:00 AM">01:00 AM</option>
													<option  @if ($key->closing_time == "02:00 AM")
														selected
													@endif value="02:00 AM">02:00 AM</option>
													<option   @if ($key->closing_time == "03:00 AM")
														selected
													@endif
													value="03:00 AM">03:00 AM</option>
													<option   @if ($key->closing_time == "04:00 AM")
														selected
													@endif value="04:00 AM">04:00 AM</option>
													<option  @if ($key->closing_time == "05:00 AM")
														selected
													@endif value="05:00 AM">05:00 AM</option>
													<option  @if ($key->closing_time == "06:00 AM")
														selected
													@endif value="06:00 AM">06:00 AM</option>
													<option  @if ($key->closing_time == "07:00 AM")
														selected
													@endif value="07:00 AM">07:00 AM</option>
													<option   @if ($key->closing_time == "08:00 AM")
														selected
													@endif value="08:00 AM">08:00 AM</option>
													<option  @if ($key->closing_time == "09:00 AM")
														selected
													@endif value="09:00 AM">09:00 AM</option>
													<option  @if ($key->closing_time == "10:00 AM")
														selected
													@endif value="10:00 AM">10:00 AM</option>
													<option  @if ($key->closing_time == "11:00 AM")
														selected
													@endif value="11:00 AM">11:00 AM</option>
													<option  @if ($key->closing_time == "12:00 AM")
														selected
													@endif value="12:00 PM">12:00 PM</option>
													<option  @if ($key->closing_time == "01:00 PM")
														selected
													@endif value="01:00 PM">01:00 PM</option>
													<option @if ($key->closing_time == "02:00 PM")
														selected
													@endif value="02:00 PM">02:00 PM</option>
													<option @if ($key->closing_time == "03:00 PM")
														selected
													@endif value="03:00 PM">03:00 PM</option>
													<option @if ($key->closing_time == "04:00 PM")
														selected
													@endif value="04:00 PM">04:00 PM</option>
													<option @if ($key->closing_time == "05:00 PM")
														selected
													@endif value="05:00 PM">05:00 PM</option>
													<option @if ($key->closing_time == "06:00 PM")
														selected
													@endif value="06:00 PM">06:00 PM</option>
													<option @if ($key->closing_time == "07:00 PM")
														selected
													@endif value="07:00 PM">07:00 PM</option>
													<option @if ($key->closing_time == "08:00 PM")
														selected
													@endif value="08:00 PM">08:00 PM</option>
													<option @if ($key->closing_time == "09:00 PM")
														selected
													@endif value="09:00 PM">09:00 PM</option>
													<option @if ($key->closing_time == "10:00 PM")
														selected
													@endif value="10:00 PM">10:00 PM</option>
													<option @if ($key->closing_time == "11:00 PM")
														selected
													@endif value="11:00 PM">11:00 PM</option>
												</select>
										</div>
										<div class="input-field col s6">
											<select class="" name="status">

												<option
												@if ($key->status == "1")
													selected
												@endif
												value="1">Active</option>
												<option
												@if ($key->status == "2")
													selected
												@endif value="2">In-Active</option>
												<option
												@if ($key->status == "3")
													selected
												@endif value="3">Sold</option>
												<option
												@if ($key->status == "4")
													selected
												@endif value="4">Admin Active</option>
											</select>
										</div>
										<div class="row">
											<br>
										</div>



										<div class="col s6">
                      <div class="input-field">
												<textarea name="description" rows="8">{{$key->description}}</textarea>
										</div>
										</div>
										<div class="col s6">
                      <div class="input-field">
												<textarea name="about" rows="8">{{$key->about}}</textarea>
										</div>
										</div>
										<div class="col s6">
											<div class="input-field">
												<input type="text" name="facebook" value="{{$key->facebook}}" placeholder="www.facebook.com/directory">
											</div>
										</div>
										<div class="col s6">
											<div class="input-field">
												<input type="text" name="google" value="{{$key->google}}" placeholder="www.google.com/directory">
											</div>
										</div>
										<div class="col s6">
											<div class="input-field">
												<input type="text" name="twitter" value="{{$key->twitter}}" placeholder="www.twitter.com/directory">
											</div>
										</div>
										<div class="col s6">
											<div class="input-field">
												<input type="text" name="google_map" value="{{$key->google_map}}" placeholder="Google Map">
											</div>
										</div>
										<div class="col s6">
											<div class="input-field">
												<input type="text" name="vr_map" value="{{$key->vr_map}}" placeholder="360 Degree View">
											</div>
										</div>
										<div class="row">
											<br>
										</div>
										<p>GAllery</p>
										<div class="col s12">
                      <div class="input-field">
												<input type="hidden" name="" value="{{$key->gallery}}">
												@php
													$gallery = ListingsGalley::where('listing_id' ,$key->id)->get();
													// dd($gallery);
												@endphp
												@if (count($gallery) > 0)
													@foreach ($gallery as $image)
															{{-- <img src="{{ $image->image }}" style="width: 100px;" alt=""> --}}
															<div id="del{{ $image->id }}" style="display: inline-block;" class="img__wrap">
																<img src="{{$image->image}}" style="max-width: 100px;" alt="" class="img__img">
																{{-- <span onclick="del({{ $gallery->id }})">Delete</span> --}}
																<div class="img__description" onclick="del({{ $image->id }})">X</div>

															</div>
														@endforeach
												@endif
												{{-- <a href="/luckylu/gallerydelete/{{$key->id}}"> <img src="{{config('app.url')}}/listings/{{$key->gallery}}"  alt="" width="150" height="100"> Delete </a> --}}
											</div>
										</div>
										@if ($key->cover != '')
											{{-- COver --}}
											<p>COver</p>
											<div class="col s12">
												<div id="cov{{ $key->id }}" style="display: inline-block;" class="img__wrap">
													<img src="{{$key->cover}}" style="max-width: 100px;" alt="" class="img__img">
													{{-- <span onclick="del({{ $gallery->id }})">Delete</span> --}}
													<div class="img__description" onclick="cov({{ $key->id }})">X</div>

												</div>
												{{-- <div class="input-field">
													<img src="{{$key->cover}}"  alt="" style="width: 100px;">
												</div> --}}
											</div>
										@endif




									<div class="row">
										<div class="input-field col s12 v2-mar-top-40">
											{{-- {{Form::hidden('_method','PUT')}} --}}
                      <input type="submit" name="submit" value="Save" class="btn btn-success">
                      </div>
									</div>
								</form>
								{{-- {!! Form::close() !!} --}}
							</div>
						</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--== BOTTOM FLOAT ICON ==-->
	<section>
		<div class="fixed-action-btn vertical">
			<a class="btn-floating btn-large red pulse"> <i class="large material-icons">mode_edit</i> </a>
			<ul>
				<li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a> </li>
				<li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a> </li>
				<li><a class="btn-floating green"><i class="material-icons">publish</i></a> </li>
				<li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a> </li>
			</ul>
		</div>
	</section>
	<!--SCRIPT FILES-->
	<script src="{{config('app.url')}}/js/jquery.min.js"></script>
	<script src="{{config('app.url')}}/js/bootstrap.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/materialize.min.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/custom.js"></script>
	<script type="text/javascript">
	function del(e) {
					var id = e;
					$.ajax({
						headers: {
							 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					 },
					type: "POST",
					url: '/api/deleteListingGallery',
					data: {id: id},
						success: function( data ) {
								// console.log(data);
								if (data == 'D') {
									var delId = 'del'+id;
									$("#"+delId+"").remove();
								}
						}
					});

	}
	function cov(e) {
					var id = e;
					// console.log(id);
					$.ajax({
						headers: {
							 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					 },
					type: "POST",
					url: '/api/deleteListingCover',
					data: {id: id},
						success: function( data ) {
								console.log(data);
								if (data == 'D') {
									var delId = 'cov'+id;
									$("#"+delId+"").remove();
								}
						}
					});

	}
	</script>
</body>


<!-- Mirrored from rn53themes.net/themes/demo/directory/admin-list-add.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:48:54 GMT -->
</html>
