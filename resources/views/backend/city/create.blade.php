@php
use App\State;
@endphp
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from rn53themes.net/themes/demo/directory/admin-list-add.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:48:54 GMT -->
<head>
	<title>Create City | Firtoo</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- FAV ICON(BROWSER TAB ICON) -->
	<link rel="shortcut icon" href="{{config('app.url')}}/images/fav.ico" type="image/x-icon">
	<!-- GOOGLE FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
	<!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="{{config('app.url')}}/css/font-awesome.min.css">
	<!-- ALL CSS FILES -->
	<link href="{{config('app.url')}}/css/materialize.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/style.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<link href="{{config('app.url')}}/css/responsive.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="{{config('app.url')}}/js/html5shiv.js"></script>
	<script src="{{config('app.url')}}/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--== MAIN CONTRAINER ==-->
	@include('backend.includes.header')
	<!--== BODY CONTNAINER ==-->
	<div class="container-fluid sb2">
		<div class="row">
			@include('backend.includes.sidebar')
			<!--== BODY INNER CONTAINER ==-->
			<div class="sb2-2">
				<!--== breadcrumbs ==-->
				<div class="sb2-2-2">
					<ul>
						<li><a href="/admin"><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
						<li class="active-bre"><a href="#"> Add City</a> </li>
						<li class="page-back"><a href="/admin"><i class="fa fa-backward" aria-hidden="true"></i> Back</a> </li>
					</ul>
				</div>
				<div class="tz-2 tz-2-admin">
					<div class="tz-2-com tz-2-main">
						<h4>Add New City</h4>

						<!-- Dropdown Structure -->
						<div class="split-row">
							<div class="col-md-12">
								<div class="box-inn-sp ad-inn-page">
									<div class="tab-inn ad-tab-inn">
						<div class="hom-cre-acc-left hom-cre-acc-right">
							<div class="">
                <form class="" action="{{action('CityController@store')}}" method="POST" >
                    {{ csrf_field() }}
										<div class="row">
											<div class="input-field col s6">
												<select class="" name="state" required>
													<option value="" disabled selected>Select</option>
													@php
														$states_list = State::all();
													@endphp
													@foreach ($states_list as $key)
														<option value="{{$key->name}}">{{$key->name}}</option>
													@endforeach
												</select>
												@if ($errors->has('state'))
												<span class="help-block">
														<strong>{{ $errors->first('state') }}</strong>
												</span>
												@endif
											</div>
											<div class="input-field col s6">
												<input  type="text" name="city" value="{{old('city')}}" class="validate" required>
												<label for="first_name">City Name</label>
												@if ($errors->has('city'))
												<span class="help-block">
														<strong>{{ $errors->first('city') }}</strong>
												</span>
												@endif
											</div>
										</div>

									<div class="row">
										<div class="input-field col s6">
											<input type="text" name="pincode" class="validate" value="{{old('pincode')}}" required>
											<label for="first_name">Pincode</label>
											@if ($errors->has('pincode'))
											<span class="help-block">
													<strong>{{ $errors->first('pincode') }}</strong>
											</span>
											@endif
										</div>
										<div class="col s3">
                      <div class="input-field">
												<input type="submit" name="submit" value="Submit" class="btn btn-success">
										</div>
										</div>
										<br><br><br><br><br><br>
										<br><br><br><br><br><br>
										<br><br><br><br><br><br>
										<br><br><br><br><br><br>
									</div>

								</form>
							</div>
						</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--== BOTTOM FLOAT ICON ==-->
	<section>
		<div class="fixed-action-btn vertical">
			<a class="btn-floating btn-large red pulse"> <i class="large material-icons">mode_edit</i> </a>
			<ul>
				<li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a> </li>
				<li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a> </li>
				<li><a class="btn-floating green"><i class="material-icons">publish</i></a> </li>
				<li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a> </li>
			</ul>
		</div>
	</section>
	<!--SCRIPT FILES-->
	<script src="{{config('app.url')}}/js/jquery.min.js"></script>
	<script src="{{config('app.url')}}/js/bootstrap.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/materialize.min.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/custom.js"></script>
</body>


<!-- Mirrored from rn53themes.net/themes/demo/directory/admin-list-add.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:48:54 GMT -->
</html>
