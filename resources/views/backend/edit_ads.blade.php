@php
	use App\Category;
	use App\Subcategories;
@endphp
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from rn53themes.net/themes/demo/directory/admin-list-add.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:48:54 GMT -->
<head>
	<title>Edit Ads | Firtoo</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- FAV ICON(BROWSER TAB ICON) -->
	<link rel="shortcut icon" href="{{config('app.url')}}/images/fav.ico" type="image/x-icon">
	<!-- GOOGLE FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
	<!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="{{config('app.url')}}/css/font-awesome.min.css">
	<!-- ALL CSS FILES -->
	<link href="{{config('app.url')}}/css/materialize.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/style.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<link href="{{config('app.url')}}/css/responsive.css" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<style>
	/* quick reset */
* {
margin: 0;
padding: 0;
border: 0;
}

/* relevant styles */
.img__wrap {
position: relative;
/* height: 200px; */
width: 100px;
}

.img__description {
position: absolute;
top: 0;
bottom: 0;
left: 0;
right: 0;
background: rgba(29, 106, 154, 0.72);
color: #fff;
visibility: hidden;
opacity: 0;
text-align: center;
line-height: 6;

/* transition effect. not necessary */
transition: opacity .2s, visibility .2s;
cursor: pointer;
}

.img__wrap:hover .img__description {
visibility: visible;
opacity: 1;
}
</style>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="{{config('app.url')}}/js/html5shiv.js"></script>
	<script src="{{config('app.url')}}/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--== MAIN CONTRAINER ==-->
	@include('backend.includes.header')
	<!--== BODY CONTNAINER ==-->
	<div class="container-fluid sb2">
		<div class="row">
			@include('backend.includes.sidebar')
			<!--== BODY INNER CONTAINER ==-->
			<div class="sb2-2">
				<!--== breadcrumbs ==-->
				<div class="sb2-2-2">
					<ul>
						<li><a href="/luckylu"><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
						<li class="active-bre"><a href="#"> Edit Ads</a> </li>
						<li class="page-back"><a href="/luckylu"><i class="fa fa-backward" aria-hidden="true"></i> Back</a> </li>
					</ul>
				</div>
				<div class="tz-2 tz-2-admin">
					<div class="tz-2-com tz-2-main">
						<h4>Edit Ads</h4>

						<!-- Dropdown Structure -->
						<div class="split-row">
							<div class="col-md-12">
								<div class="box-inn-sp ad-inn-page">
									<div class="tab-inn ad-tab-inn">
						<div class="hom-cre-acc-left hom-cre-acc-right">
							<div class="">
                {{-- {!! Form::open(['action' => ['AdminReportController@update',$key->id], 'method'=> 'POST']) !!} --}}
								<form class="" action="{{route('updateaddpost')}}" method="POST">
									{{ csrf_field() }}
									<div class="row">
										{{-- <input name="_method" type="hidden" value="PUT"> --}}
										<input  type="hidden" name="id" class="validate" value = "{{$key->id}}">
										<input  type="hidden" name="user_id" class="validate" value = "{{$key->user_id}}">
										<div class="input-field col s6">
											<select name="category_id" disabled>
												<option value="" disabled selected>Select Category</option>
												@php
													$categories = Category::all();
												@endphp
												@foreach ($categories as $category)

													<option
													@if ($category->id == $key->category_id)
								            selected
								          @endif
													value="{{$category->id}}">{{$category->name}}</option>
												@endforeach
											</select>
										</div>
										<div class="input-field col s6">
											<select name="subcategory_id" disabled>
												<option value="" disabled selected>Select SubCategory</option>
												@php
													$subcategories = Subcategories::all();
												@endphp
												@foreach ($subcategories as $subcategory)

													<option
													@if ($subcategory->id == $key->subcategory_id)
								            selected
								          @endif
													value="{{$subcategory->id}}">{{$subcategory->name}}</option>
												@endforeach
											</select>
										</div>

									</div>
									<div class="row">
										<div class="col s6">
                      <div class="input-field">
												<input id="name" type="text" name="item" class="validate" value = "{{$key->item}}" disabled>
										</div>
										</div>
										<div class="input-field col s6">
											<input id="name" type="text" name="price" class="validate" value = "{{$key->price}}" disabled>
										</div>

									</div>
									<div class="row">

										<div class="input-field col s6">
											<select class="" name="status">

												<option
												@if ($key->status == "1")
													selected
												@endif
												value="1">Active</option>
												<option
												@if ($key->status == "2")
													selected
												@endif value="2">In-Active</option>
												<option
												@if ($key->status == "3")
													selected
												@endif value="3">Sold</option>
												<option
												@if ($key->status == "4")
													selected
												@endif value="4">Admin Active</option>
											</select>
										</div>
										<div class="input-field col s6">
											<input id="name" disabled type="text" name="bought_year" maxlength="4" class="validate" value = "{{$key->bought_year}}">
										</div>

									</div>
									<div class="row">
										<div class="col s6">
                      <div class="input-field">
												<textarea disabled name="description" rows="8">{{$key->description}}</textarea>
										</div>
										</div>

										<div class="col s6">
                      <div class="input-field">
												{{-- <input type="hidden" name="" value="{{$key->image}}"> --}}
												@if ($key->image)
													{{-- <a href="/luckylu/delete/{{$key->id}}/{{"image"}}"> <img src="{{$key->image}}"  alt="" width="150" height="100"> Delete </a> --}}
													<div id="img1" style="display: inline-block;" class="img__wrap">
														<img src="{{$key->image}}" style="max-width: 100px;" alt="" class="img__img">
														{{-- <span onclick="del({{ $gallery->id }})">Delete</span> --}}
														<div class="img__description" onclick="img1({{ $key->id }})">X</div>

													</div>
												@endif
											</div>
										</div>
										</div>
										<div class="row">
										<div class="col s6">
											@if ($key->image2)
												{{-- <a href="/luckylu/delete/{{$key->id}}/{{"image2"}}"> <img src="{{$key->image2}}"  alt="" width="150" height="100"> Delete </a> --}}
												<div id="img2" style="display: inline-block;" class="img__wrap">
													<img src="{{$key->image2}}" style="max-width: 100px;" alt="" class="img__img">
													{{-- <span onclick="del({{ $gallery->id }})">Delete</span> --}}
													<div class="img__description" onclick="img2({{ $key->id }})">X</div>

												</div>
											@endif
										</div>
										<div class="col s6">
											@if ($key->image3)
												{{-- <a href="/luckylu/delete/{{$key->id}}/{{"image3"}}"> <img src="{{$key->image3}}"  alt="" width="150" height="100"> Delete </a> --}}
												<div id="img3" style="display: inline-block;" class="img__wrap">
													<img src="{{$key->image3}}" style="max-width: 100px;" alt="" class="img__img">
													{{-- <span onclick="del({{ $gallery->id }})">Delete</span> --}}
													<div class="img__description" onclick="img3({{ $key->id }})">X</div>

												</div>
											@endif
										</div>


									</div>
									<div class="row">

									</div>


									<div class="row">
										<div class="input-field col s12 v2-mar-top-40">
											{{-- {{Form::hidden('_method','PUT')}} --}}
                      <input type="submit" name="submit" value="Submit Category" class="btn btn-success">
                      </div>
									</div>
								</form>
								{{-- {!! Form::close() !!} --}}
							</div>
						</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--== BOTTOM FLOAT ICON ==-->
	<section>
		<div class="fixed-action-btn vertical">
			<a class="btn-floating btn-large red pulse"> <i class="large material-icons">mode_edit</i> </a>
			<ul>
				<li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a> </li>
				<li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a> </li>
				<li><a class="btn-floating green"><i class="material-icons">publish</i></a> </li>
				<li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a> </li>
			</ul>
		</div>
	</section>
	<!--SCRIPT FILES-->
	<script src="{{config('app.url')}}/js/jquery.min.js"></script>
	<script src="{{config('app.url')}}/js/bootstrap.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/materialize.min.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/custom.js"></script>
	<script type="text/javascript">
	function img1(e) {
					var id = e;
					// console.log(id);
					$.ajax({
						headers: {
							 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					 },
					type: "POST",
					url: '/api/deleteClassifyImgOne',
					data: {id: id},
						success: function( data ) {
								console.log(data);
								if (data == 'D') {
									// var delId = 'cov'+id;
									$("#img1").remove();
								}
						}
					});

	}
	function img2(e) {
					var id = e;
					// console.log(id);
					$.ajax({
						headers: {
							 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					 },
					type: "POST",
					url: '/api/deleteClassifyImgTwo',
					data: {id: id},
						success: function( data ) {
								console.log(data);
								if (data == 'D') {
									// var delId = 'cov'+id;
									$("#img2").remove();
								}
						}
					});

	}
	function img3(e) {
					var id = e;
					// console.log(id);
					$.ajax({
						headers: {
							 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					 },
					type: "POST",
					url: '/api/deleteClassifyImgThree',
					data: {id: id},
						success: function( data ) {
								console.log(data);
								if (data == 'D') {
									// var delId = 'cov'+id;
									$("#img3").remove();
								}
						}
					});

	}
	</script>
</body>


<!-- Mirrored from rn53themes.net/themes/demo/directory/admin-list-add.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:48:54 GMT -->
</html>
