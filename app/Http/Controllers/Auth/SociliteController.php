<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SociliteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function savefacebookform()
    {
      return view('frontend.facebookRegister');
    }

    public function savefacebook(Request $request)
    {
      // return $request;
      $mobile = $request['mobile'];
      $rand = rand(111111, 999999);
      // $this->redirectTo = "/verifymobile/$mobile";
        User::create([
            'facebook_id' => $request['facebook_id'],
            'name' => $request['name'],
            'mobile' => $request['mobile'],
            'email' => $request['email'],
            'status' => 0,
            'otp' => $rand,
            'password' => bcrypt($request['password']),
        ]);

        return redirect("/verifymobile/$mobile");

    }

    public function savegoogleform()
    {
      return view('frontend.googleRegister');
    }

    public function savegoogle(Request $request)
    {
      // return $request;
      $mobile = $request['mobile'];
      $rand = rand(111111, 999999);
      // $this->redirectTo = "/verifymobile/$mobile";
        User::create([
            'google_id' => $request['google_id'],
            'name' => $request['name'],
            'mobile' => $request['mobile'],
            'email' => $request['email'],
            'status' => 0,
            'otp' => $rand,
            'password' => bcrypt($request['password']),
        ]);

        return redirect("/verifymobile/$mobile");

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show(user $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(user $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, user $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(user $user)
    {
        //
    }
}
