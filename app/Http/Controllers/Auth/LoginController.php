<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use Exception;
use App\User;
use Session;

use Illuminate\Http\Request;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
   {
       $this->middleware('guest',['except'=>['logout','UserLogout']]);
   }

   // public function redirectToProvider()
   //  {
   //      return Socialite::driver('facebook')->redirect();
   //  }
   //
    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
        } catch (Exception $e) {
            return redirect('login/facebook');
        }

        // return var_dump($user);
        $authUser = User::where('facebook_id', $user->id)->first();
        // $authUser = $this->findOrCreateUser($user);

        if ($authUser) {
          // return $authUser;
          if ($authUser->status != 1) {
            return redirect("/verifymobile/$authUser->mobile/");
          } else {
            if (Auth::login($authUser)) {
              return redirect('home');
            } else {
              return redirect('login');
            }
          }

        } else {
          Session::put('facebook_id', $user->id);
          Session::put('name', $user->name);
          Session::put('email', $user->email);
          Session::put('facebook_id', $user->id);
          Session::put('avatar', $user->avatar);
          return redirect('/facebookregister');
        }


        // $value = Session::get('name');
        // return $value;

        // Auth::login($authUser, true);


    }

    public function handleProviderCallbackGoogle()
    {
        try {
            $user = Socialite::driver('google')->user();
        } catch (Exception $e) {
            return redirect('/login');
        }

        // return var_dump($user);
        $authUser = User::where('google_id', $user->id)->first();
        // $authUser = $this->findOrCreateUser($user);

        if ($authUser) {
          // return $authUser;
          if ($authUser->status != 1) {
            return redirect("/verifymobile/$authUser->mobile/");
          } else {
            if (Auth::login($authUser)) {
              return redirect('home');
            } else {
              return redirect('login');
            }
          }

        } else {
          Session::put('google_id', $user->id);
          Session::put('name', $user->name);
          Session::put('email', $user->email);
          Session::put('google_id', $user->id);
          Session::put('avatar', $user->avatar);
          return redirect('/googleregister');
        }


        // $value = Session::get('name');
        // return $value;

        // Auth::login($authUser, true);


    }

    // private function findOrCreateUser($facebookUser)
    // {
    //     $authUser = User::where('facebook_id', $facebookUser->id)->first();
    //
    //     if ($authUser){
    //         return $authUser;
    //     }
    //
    //     // return $authUser;
    //
    //
    //
    //
    //
    //     // return User::create([
    //     //     'name' => $facebookUser->name,
    //     //     'email' => $facebookUser->email,
    //     //     'facebook_id' => $facebookUser->id,
    //     //     'avatar' => $facebookUser->avatar
    //     // ]);
    // }

   public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

   public function redirectToProviderGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    // public function handleProviderCallback()
    // {
    //     $user = Socialite::driver('facebook')->user();
    //
    //     var_dump($user);
    //     return $user->name;
    // }



   protected function credentials(Request $request)
   {
       return ['email' => $request->{$this->username()}, 'password' => $request->password, 'status' => '1'];
   }

   protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];

        // Load user from database
        $user = User::where($this->username(), $request->{$this->username()})->first();

        // Check if user was successfully loaded, that the password matches
        // and active is not 1. If so, override the default error message.
        if ($user && \Hash::check($request->password, $user->password) && $user->status != 1) {
            $errors = [$this->username() => trans('auth.notactivated')];
        }

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

   public function UserLogout()
   {
       Auth::guard('web')->logout();
       return redirect('/');
   }
}
