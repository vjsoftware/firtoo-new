<?php

namespace App\Http\Controllers;

use App\Subcategories;
use Illuminate\Http\Request;

class SubcategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $list = Subcategories::all();
      return view('backend.subcategories.list')->with('list', $list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.subcategories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // return $request;
      $this->validate($request,[
        'category_id' => 'required',
        'name' => 'required',
      ]);
      $add = new SubCategories;
      $add->category_id = $request->category_id;
      $add->name = $request->name;
      $add->save();
      return redirect('/luckylu/subcategories');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subcategories  $subcategories
     * @return \Illuminate\Http\Response
     */
    public function show(Subcategories $subcategories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subcategories  $subcategories
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $list = Subcategories::find($id);
     return view('backend.subcategories.edit')->with('key', $list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subcategories  $subcategories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request,[
        'category_id' => 'required',
        'name' => 'required',
      ]);
      $add =  SubCategories::find($id);
      $add->category_id = $request->category_id;
      $add->name = $request->name;
      $add->save();
      return redirect('/luckylu/subcategories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subcategories  $subcategories
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      // return  $id;
      $destroy_info = Subcategories::find($id);
      $destroy_info->delete();
      return redirect('/luckylu/subcategories');
    }
}
