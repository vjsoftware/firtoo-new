<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Listing;
use App\Addposting;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.home');
    }


    public function show($id)
    {
      $list_id = Listing::where('category_id', $id)->where('status', 4)->get();
      // return $list_id;
        $data = [
          'key' => $id,
          'list' => $list_id
        ];

        return view('frontend.list')->with('data', $data);
    }
    public function result($id)
    {
        $list_id = Listing::find($id);
        return view('frontend.list_details')->with('key',$list_id);
    }

    public function slug($city, $slug)
    {
      // return 'slug';
        $list_id = Listing::where('city', $city)->where('slug', $slug)->get();
        // return $list_id;
        return view('frontend.list_details')->with('key',$list_id);
    }

    public function alllistings()
    {
      $alllists = Listing::where('status', 4)->orderBy('id', 'DESC')->get();
      return view('frontend.AllListings')->with('lists', $alllists);
    }
    public function dashboard()
    {
      return view('frontend.dashboard');
    }

}
