<?php

namespace App\Http\Controllers;

use App\Category;
use App\Subcategories;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
  // public function __construct()
  // {
  //     $this->middleware('auth:admin');
  // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Category::all();
        return view('backend.categories.list')->with('list', $list);
    }

    public function categoriesAll()
    {
        $list = Category::all();
        // return view('backend.categories.list')->with('list', $list);
        return $list;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $this->validate($request,[
          'name' => 'required',
          'image' => 'required|mimes:jpeg,jpg,png|max:1000',
        ]);
        // if ($request->hasFile('image')) {
        //
        //   $file = $request->image;
        //   $fullPhotoNameWithExt = $file->getClientOriginalName();
        //   $fileName = pathinfo($fullPhotoNameWithExt, PATHINFO_FILENAME);
        //   $fileExt = $file->getClientOriginalExtension();
        //   $photoToSave = $fileName.'_'.time().'.'. $fileExt;
        //   $path = $file->move('categories', $photoToSave);
        // }

        if ($request->hasFile('image')) {

          $file = $request->image;
          $imageFileName = time() . '.' . $file->getClientOriginalExtension();
          $location = env('AWS_DEFAULT_REGION');
          $bucket = env('AWS_BUCKET');
          $filePath = '/category/' . $imageFileName;
          $imageFileNameFull = "https://s3.$location.amazonaws.com/$bucket$filePath";
          $s3 = \Storage::disk('s3');
          $filePath = '/category/' . $imageFileName;
          $s3->put($filePath, file_get_contents($file), 'public');
          $photoToSave = $imageFileNameFull;
        }else {
          $photoToSave = ' ';
        }
          $add = new category();
          $add->name = $request->name;
            $add->image = $photoToSave;
            $add->save();
            return redirect('/luckylu/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $list = Category::find($id);
     return view('backend.categories.edit')->with('key', $list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      // return $request;
      $this->validate($request,[
        'name' => 'required',

      ]);
      $edit = Category::find($id);
      // if ($request->hasFile('image')) {
      //
      //   $file = $request->image;
      //   $fullPhotoNameWithExt = $file->getClientOriginalName();
      //   $fileName = pathinfo($fullPhotoNameWithExt, PATHINFO_FILENAME);
      //   $fileExt = $file->getClientOriginalExtension();
      //   $photoToSave = $fileName.'_'.time().'.'. $fileExt;
      //   $path = $file->move('categories', $photoToSave);
      //   $edit->image = $photoToSave;
      // }
      if ($request->hasFile('image')) {

        $file = $request->image;
        $imageFileName = time() . '.' . $file->getClientOriginalExtension();
        $location = env('AWS_DEFAULT_REGION');
        $bucket = env('AWS_BUCKET');
        $filePath = '/category/' . $imageFileName;
        $imageFileNameFull = "https://s3.$location.amazonaws.com/$bucket$filePath";
        $s3 = \Storage::disk('s3');
        $filePath = '/category/' . $imageFileName;
        $s3->put($filePath, file_get_contents($file), 'public');
        $photoToSave = $imageFileNameFull;
        $edit->image = $photoToSave;
      }

      $edit->name = $request->name;
          $edit->save();
          return redirect('/luckylu/categories/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $destroy_info = Category::find($id);
      $destroy_info->delete();
      return redirect('/luckylu/categories');
    }

    public function getSubCategories($id)
    {
      $subCategories = Subcategories::where('category_id', $id)->get();
      return $subCategories;
    }
}
