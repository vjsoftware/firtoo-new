<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use Auth;
use App\User;

class profilePicController extends Controller
{
  public function update_photo_crop(Request $request) {
    // return $request;
  $cropped_value = $request->input("cropped_value"); //// Width,height,x,y,rotate for cropping
  $cp_v = explode("," ,$cropped_value); /// Explode width,height,x etc
  $file = $request->file;
  $userId = Auth::user()->id;
  $userName = Auth::user()->name;
  $userImage = Auth::user()->image;
  $time = time();
  $file_name = "{$userName}-{$userId}-{$time}.jpg";
  if ($request->hasFile('file')) {
      // if ($userImage != '') {
      //   $siteUrl = config('app.url');
      //   $deleteFile = "./profile/{$userImage}";
      //   unlink($deleteFile);
      // }
     $path = $file->storeAs("original/path/" , $file_name); // Original Image Path
     $img = Image::make($file->getRealPath());
     $path2 = public_path("profile/$file_name"); ///  Cropped Image Path
     $img->rotate($cp_v[4] * -1);  /// Rotate Image
     // return $path2;
     $imageNew = $img->crop($cp_v[0],$cp_v[1],$cp_v[2],$cp_v[3]);
     $image_normal = $imageNew->stream();
     // return $image_normal;
     // return $imageNew;
     // $img->crop($cp_v[0],$cp_v[1],$cp_v[2],$cp_v[3])->save($path2); // Crop and Save
     // S3
     $imageFileName = $file_name;
     // return $file;
     $location = env('AWS_DEFAULT_REGION');
     $bucket = env('AWS_BUCKET');
     $filePath = '/profile_pic/' . $imageFileName;
     $imageFileNameFull = "https://s3.$location.amazonaws.com/$bucket$filePath";
     $s3 = \Storage::disk('s3');
     $filePath = '/profile_pic/' . $imageFileName;
     $s3->put($filePath, $image_normal->__toString(), 'public');
     $photoToSave = $imageFileNameFull;
     // S3 Close
     $add = User::find($userId);
     $add->image = $photoToSave;
     $add->save();
     echo $photoToSave;
    }
  }
}
