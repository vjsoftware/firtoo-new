<?php

namespace App\Http\Controllers;

use App\Addposting;
use App\Subcategories;
use Auth;
use Illuminate\Http\Request;
use Storage;

class AddpostingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except' => ['allads', 'showads']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Addposting::all();
        return view('frontend.classifiedslist')->with('list', $list);
    }
    public function allads()
    {
        $list = Addposting::where('status', 4)->get();
        return view('frontend.showallads')->with('list', $list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      // echo "string";
        // die('rr');
        // return 'll';
        return view('frontend.addpostings');
    }

    public function add()
    {
      return 'ooo';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $this->validate($request, [
            'category' => 'required',
            // 'subCategory' => 'required',
            'item' => 'required|min:2',
            'price' => 'required|min:1',
            'status' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png|max:5000',
            'image2' => 'mimes:jpeg,jpg,png|max:5000',
            'image' => 'mimes:jpeg,jpg,png|max:5000',
            'description' => 'required|min:5',
        ]);

        if ($request->hasFile('image')) {

          $file = $request->image;
          // $fullPhotoNameWithExt = $file->getClientOriginalName();
          // $fileName = pathinfo($fullPhotoNameWithExt, PATHINFO_FILENAME);
          // $fileExt = $file->getClientOriginalExtension();
          // $photoToSave = $fileName.'_'.time().'.'. $fileExt;
          // $path = $file->move('posts', $photoToSave);
          $imageFileName = time() . '.' . $file->getClientOriginalExtension();
          // return $file;
          $location = env('AWS_DEFAULT_REGION');
          $bucket = env('AWS_BUCKET');
          $filePath = '/classifieds/' . $imageFileName;
          $imageFileNameFull = "https://s3.$location.amazonaws.com/$bucket$filePath";
          $s3 = \Storage::disk('s3');
          $filePath = '/classifieds/' . $imageFileName;
          $s3->put($filePath, file_get_contents($file), 'public');
          $photoToSave = $imageFileNameFull;
        } else {
          $photoToSave = '';
        }


        if ($request->hasFile('image2')) {

          $file = $request->image2;
          // $fullPhotoNameWithExt2 = $file2->getClientOriginalName();
          // $fileName2 = pathinfo($fullPhotoNameWithExt2, PATHINFO_FILENAME);
          // $fileExt2 = $file2->getClientOriginalExtension();
          // $photoToSave2 = $fileName2.'_'.time().'.'. $fileExt2;
          // $path2 = $file2->move('posts', $photoToSave2);
          $imageFileName = time() . '.' . $file->getClientOriginalExtension();
          $location = env('AWS_DEFAULT_REGION');
          $bucket = env('AWS_BUCKET');
          $filePath = '/classifieds/' . $imageFileName;
          $imageFileNameFull = "https://s3.$location.amazonaws.com/$bucket$filePath";
          $s3 = \Storage::disk('s3');
          $filePath = '/classifieds/' . $imageFileName;
          $s3->put($filePath, file_get_contents($file), 'public');
          $photoToSave2 = $imageFileNameFull;
        } else {
          $photoToSave2 = '';
        }

        if ($request->hasFile('image3')) {
          // return 'ppppp3';

          $file = $request->image3;
          // $fullPhotoNameWithExt3 = $file3->getClientOriginalName();
          // $fileName3 = pathinfo($fullPhotoNameWithExt3, PATHINFO_FILENAME);
          // $fileExt3 = $file3->getClientOriginalExtension();
          // $photoToSave3 = $fileName3.'_'.time().'.'. $fileExt3;
          // $path3 = $file3->move('posts', $photoToSave3);
          $imageFileName = time() . '.' . $file->getClientOriginalExtension();
          $location = env('AWS_DEFAULT_REGION');
          $bucket = env('AWS_BUCKET');
          $filePath = '/classifieds/' . $imageFileName;
          $imageFileNameFull = "https://s3.$location.amazonaws.com/$bucket$filePath";
          $s3 = \Storage::disk('s3');
          $filePath = '/classifieds/' . $imageFileName;
          $s3->put($filePath, file_get_contents($file), 'public');
          $photoToSave3 = $imageFileNameFull;
        } else {
          $photoToSave3 = '';
        }
        // return $request;

        $add = new Addposting();
        $add->user_id = Auth::user()->id;
        $add->category_id = $request->category;
        $add->subcategory_id = $request->subCategory;
        $add->city = $request->city;
        $add->mobile = Auth::user()->mobile;
        $add->item = $request->item;
        $add->price = $request->price;
        $add->description = $request->description;
        $add->status = $request->status;
        $add->bought_year = $request->bought_year;
        $add->image = $photoToSave;
        $add->image2 = $photoToSave2;
        $add->image3 = $photoToSave3;
        $add->other_city = $request->other_city;
        $add->save();
        return redirect('/addposting');
    }
    public function SubCategoryFind(Request $request)
    {
      $subcategories = new Subcategories();
      $subcategory = Subcategories::where('category_id', $request->category_id)->get();
      return $subcategory;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Addposting  $addposting
     * @return \Illuminate\Http\Response
     */
    public function show(Addposting $addposting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Addposting  $addposting
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $check = Addposting::find($id);
      return view('frontend.edit_classified')->with('key',$check);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Addposting  $addposting
     * @return \Illuminate\Http\Response
     */
    public function update(Request  $request, $id)
    {
      $this->validate($request, [
          'category_id' => 'required',
          'item' => 'required',
          'price' => 'required',
          'status' => 'required',
      ]);

      $edit =  Addposting::find($id);
        // return $edit->description;
      if ($request->hasFile('image')) {
        // $fullPhotoNameWithExt1 = $file1->getClientOriginalName();
        // $fileName1 = pathinfo($fullPhotoNameWithExt1, PATHINFO_FILENAME);
        // $fileExt1 = $file1->getClientOriginalExtension();
        // $photoToSave1 = $fileName1.'_'.time().'.'. $fileExt1;
        // $path1 = $file1->move('posts', $photoToSave1);
        if ($edit['image']) {
          $split = explode('/', $edit['image']);
          $path = $split[4].'/'.$split[5];
          // return $path;
          if(Storage::disk('s3')->exists($path)) {
            Storage::disk('s3')->delete($path);
          }
        }
        $file = $request->image;

        $rand = rand(11111111, 99999999);
        $imageFileName = $rand.time() . '.' . $file->getClientOriginalExtension();
        $location = env('AWS_DEFAULT_REGION');
        $bucket = env('AWS_BUCKET');
        $filePath = '/classifieds/' . $imageFileName;
        $imageFileNameFull = "https://s3.$location.amazonaws.com/$bucket$filePath";
        // return $imageFileNameFull;
        $s3 = \Storage::disk('s3');
        $s3->put($filePath, file_get_contents($file), 'public');
        $photoToSave1 = $imageFileNameFull;
      } else {
        $photoToSave1 = $edit->image;
        // return $photoToSave1;
      }
      if ($request->hasFile('image2')) {

        // $fullPhotoNameWithExt2 = $file2->getClientOriginalName();
        // $fileName2 = pathinfo($fullPhotoNameWithExt2, PATHINFO_FILENAME);
        // $fileExt2 = $file2->getClientOriginalExtension();
        // $photoToSave2 = $fileName2.'_'.time().'.'. $fileExt2;
        // $path2 = $file2->move('posts', $photoToSave2);
        if ($edit['image2']) {
          $split = explode('/', $edit['image2']);
          $path = $split[4].'/'.$split[5];
          // return $path;
          if(Storage::disk('s3')->exists($path)) {
            Storage::disk('s3')->delete($path);
          }
        }
        $file = $request->image2;

        $rand = rand(11111111, 99999999);
        $imageFileName = $rand.time() . '.' . $file->getClientOriginalExtension();
        $location = env('AWS_DEFAULT_REGION');
        $bucket = env('AWS_BUCKET');
        $filePath = '/classifieds/' . $imageFileName;
        $imageFileNameFull = "https://s3.$location.amazonaws.com/$bucket$filePath";
        // return $imageFileNameFull;
        $s3 = \Storage::disk('s3');
        $s3->put($filePath, file_get_contents($file), 'public');
        $photoToSave2 = $imageFileNameFull;

      }
      else {
        $photoToSave2 = $edit->image2;
      }
      if ($request->hasFile('image3')) {

        // $fullPhotoNameWithExt3 = $file3->getClientOriginalName();
        // $fileName3 = pathinfo($fullPhotoNameWithExt3, PATHINFO_FILENAME);
        // $fileExt3 = $file3->getClientOriginalExtension();
        // $photoToSave3 = $fileName3.'_'.time().'.'. $fileExt3;
        // $path3 = $file3->move('posts', $photoToSave3);
        if ($edit['image3']) {
          $split = explode('/', $edit['image3']);
          $path = $split[4].'/'.$split[5];
          // return $path;
          if(Storage::disk('s3')->exists($path)) {
            Storage::disk('s3')->delete($path);
          }
        }

        $file = $request->image3;

        $rand = rand(11111111, 99999999);
        $imageFileName = $rand.time() . '.' . $file->getClientOriginalExtension();
        $location = env('AWS_DEFAULT_REGION');
        $bucket = env('AWS_BUCKET');
        $filePath = '/classifieds/' . $imageFileName;
        $imageFileNameFull = "https://s3.$location.amazonaws.com/$bucket$filePath";
        // return $imageFileNameFull;
        $s3 = \Storage::disk('s3');
        $s3->put($filePath, file_get_contents($file), 'public');
        $photoToSave3 = $imageFileNameFull;
      } else {
        $photoToSave3 = $edit->image3;
      }

      $edit->user_id = Auth::user()->id;
      $edit->category_id = $request->category_id;
      $edit->subcategory_id = $request->subcategory_id;
      $edit->city = $request->city;
      $edit->mobile = Auth::user()->mobile;
      $edit->item = $request->item;
      $edit->price = $request->price;
      $edit->description = $request->description;
      $edit->status = $request->status;
      $edit->image = $photoToSave1;
      $edit->image2 = $photoToSave2;
      $edit->image3 = $photoToSave3;
      $edit->bought_year = $request->bought_year;
      $edit->status = 1;
      $edit->save();
      return redirect('/addposting');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Addposting  $addposting
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $destroy_info = Addposting::find($id);
      $destroy_info->delete();
      return redirect('/addposting');
    }

    public function showads($id)
    {
        $list = Addposting::find($id);
        return view('frontend.add_details')->with('list', $list);
    }
}
