<?php

namespace App\Http\Controllers;

use App\ListingReviews;
use Illuminate\Http\Request;
use Auth;

class ListingReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // return $request;
      $this->validate($request,[
        'review' => 'required',
      ]);
      $add = new ListingReviews();
      $add->listingid = $request->listingid;
      $add->userid = Auth::user()->id;
      $add->review = $request->review;
      $add->save();
      return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ListingReviews  $ListingReviews
     * @return \Illuminate\Http\Response
     */
    public function show(ListingReviews $ListingReviews)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ListingReviews  $ListingReviews
     * @return \Illuminate\Http\Response
     */
    public function edit(ListingReviews $ListingReviews)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ListingReviews  $ListingReviews
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ListingReviews $ListingReviews)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ListingReviews  $ListingReviews
     * @return \Illuminate\Http\Response
     */
    public function destroy(ListingReviews $ListingReviews)
    {
        //
    }
}
