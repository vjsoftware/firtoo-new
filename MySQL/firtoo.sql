-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 22, 2018 at 09:11 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `firtoo`
--

-- --------------------------------------------------------

--
-- Table structure for table `addpostings`
--

CREATE TABLE `addpostings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `bought_year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addpostings`
--

INSERT INTO `addpostings` (`id`, `user_id`, `category_id`, `subcategory_id`, `city`, `item`, `price`, `mobile`, `description`, `status`, `bought_year`, `image`, `image2`, `image3`, `created_at`, `updated_at`) VALUES
(1, 1, '2', 1, 'Tirupati', 'Innova', '5000000', '8056096993', 'good condition', 4, '2017', 'master_1525679042.jpg', '', '', '2018-05-07 02:14:02', '2018-05-07 02:18:39'),
(2, 1, '3', 3, 'Tirupati', 'Clinic All clear', '456', '8056096993', 'dsadsadsadsadsadsad', 4, '2017', 'master_1525679167.jpg', '', '', '2018-05-07 02:16:07', '2018-05-07 02:18:46'),
(3, 1, '5', 4, 'Tirupati', 'LG', '10000', '8056096993', 'Good Condition', 4, '2017', 'master_1525679212.jpg', '', '', '2018-05-07 02:16:52', '2018-05-07 02:18:51');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Rajesh', 'rajeshkevin.7@gmail.com', '$2y$10$hkbraNzPXUHKm8tOmWwIdO31J1KbpbqYT1vJ9oPql.FCVzOxMTOba', '1', 'xmxttUSuv4S7KXK5A4c2jgd7ox32GCpOloOgKuy71CLdIWu3MveB1FsrloFT', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Mobile', 'master_1525433009.jpg', '2018-05-04 05:53:29', '2018-05-04 05:53:29'),
(2, 'CAR', 'master_1525433031.jpg', '2018-05-04 05:53:51', '2018-05-04 05:53:51'),
(3, 'Shampoo', 'master_1525678686.jpg', '2018-05-07 01:52:24', '2018-05-07 02:08:06'),
(4, 'Air Conditionar', 'master_1525678700.jpg', '2018-05-07 01:52:48', '2018-05-07 02:08:20'),
(5, 'Computers', 'master_1525678717.jpg', '2018-05-07 01:53:35', '2018-05-07 02:08:37');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pincode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `state`, `city`, `pincode`, `created_at`, `updated_at`) VALUES
(1, 'Andhra Pradesh', 'Tirupati', '571501', '2018-05-04 05:56:44', '2018-05-04 05:56:44');

-- --------------------------------------------------------

--
-- Table structure for table `classified_reviews`
--

CREATE TABLE `classified_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `classifiedid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `review` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classified_reviews`
--

INSERT INTO `classified_reviews` (`id`, `classifiedid`, `userid`, `review`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Writing great reviews may help others that are just apt for them. Here are a few tips to write a good review:', '2018-05-07 03:57:29', '2018-05-07 03:57:29'),
(2, 2, 2, 'Good service... nice and clean rooms...', '2018-05-07 03:57:58', '2018-05-07 03:57:58'),
(3, 3, 2, 'LG computers review monitering', '2018-05-07 04:23:24', '2018-05-07 04:23:24'),
(4, 1, 1, 'Will i get it?', '2018-05-09 04:47:23', '2018-05-09 04:47:23');

-- --------------------------------------------------------

--
-- Table structure for table `listings`
--

CREATE TABLE `listings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `city_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subcategory_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opening_days` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `open_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `closing_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` text COLLATE utf8mb4_unicode_ci,
  `google` text COLLATE utf8mb4_unicode_ci,
  `twitter` text COLLATE utf8mb4_unicode_ci,
  `google_map` text COLLATE utf8mb4_unicode_ci,
  `vr_map` text COLLATE utf8mb4_unicode_ci,
  `cover` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `listings`
--

INSERT INTO `listings` (`id`, `user_id`, `title`, `phone`, `email`, `address`, `about`, `city_id`, `city`, `category_id`, `subcategory_id`, `opening_days`, `open_time`, `closing_time`, `description`, `facebook`, `google`, `twitter`, `google_map`, `vr_map`, `cover`, `gallery`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Saffron', '9600028602', 'chriscaroline11@gmail.com', 'meenakshinagar', 'hjhgjghj', NULL, 'Tirupati', '1', '2', '1', '09:00 AM', '07:00 PM', 'fgdhgfhgfhgf', NULL, NULL, NULL, NULL, NULL, '', NULL, 4, '2018-05-05 01:24:18', '2018-05-16 07:48:51'),
(5, 1, 'Accer', '8056096998', 'rameshcharan261@gmail.com', 'no 2 Gandhi Road', 'We r leading Company', NULL, 'Tirupati', '5', '4', '8', '09:00 AM', '10:00 PM', 'Good Condition Laptops', NULL, NULL, NULL, NULL, NULL, 'master_1525678843.jpg', NULL, 0, '2018-05-07 02:10:43', '2018-05-09 04:56:21'),
(6, 1, 'Head and Shoulders', '8056096993', 'rajeshkevin.7@gmail.com', 'no 2 Gandhi Road', 'fgfjgjfdjgkfjdg', NULL, 'Tirupati', '3', NULL, '8', '10:00 AM', '07:00 PM', 'good Shampoo', NULL, NULL, NULL, NULL, NULL, 'master_1525678904.jpg', '', 4, '2018-05-07 02:11:44', '2018-05-22 01:28:40'),
(7, 1, 'Hitachi', '8056096999', 'chriscaroline11@gmail.com', 'Mount Road', 'sadsadsa', NULL, 'Tirupati', '4', NULL, '8', '10:00 AM', '10:00 PM', 'sdadsad', NULL, NULL, NULL, NULL, NULL, 'master_1525678977.jpg', NULL, 4, '2018-05-07 02:12:57', '2018-05-07 02:17:48'),
(8, 1, 'Razer', '9672565556', 'rameshcharan261@gmail.com', 'no 2 Gandhi Road', 'sdasadsds', NULL, 'Tirupati', '1', '2', '8', '09:00 AM', '09:00 PM', 'sdhjshdjh', NULL, NULL, NULL, NULL, NULL, 'tirumala_1526457186.jpg', NULL, 4, '2018-05-16 07:53:06', '2018-05-16 07:59:38'),
(9, 5, 'test test test', '9490501349', 'blobyvj@gmail.com', '20-1-471/h5/g, korlagunta', 'dsfsdfsfsfsdf  fdsfsf  dfsaf  fsf f fdf fdf  ffff', NULL, 'tirupati', '2', '1', '3', '01:00 AM', '03:00 AM', 'test gfjg fsf hf fshfhfkjhf kshf khjkh', 'https://developers.facebook.com/', 'https://developers.facebook.com/', 'https://developers.facebook.com/', '20-1-471/h5/g, korlagunta', NULL, 'download_1526462227.png', NULL, 4, '2018-05-16 09:02:45', '2018-05-16 09:17:20'),
(10, 5, 'test 111', '7013089044', 'blobyvj@gmail.com', '20-1-471/h5/g', 'rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr', NULL, 'Tirupati', '1', NULL, '8', '12:00 AM', '01:00 AM', 'tttttttttttttttttttttttttttttttttttttttttttttttttttttt', 'facebook.com', 'facebook.com', 'facebook.com', 'facebook.com', NULL, '', NULL, 4, '2018-05-20 12:58:45', '2018-05-22 01:31:09'),
(11, 5, 'Image Test', '9490501349', 'blobyvj@gmail.com', '20-1-471/h5/g, korlagunta', 'sdfsdfafasf', NULL, 'Tirupati', '4', NULL, '5', '01:00 AM', '01:00 AM', 'sdfdsfsf', 'facebook.com', 'facebook.com', 'facebook.com', '20-1-471/h5/g, korlagunta', NULL, 'herda001_1526973398.jpg', NULL, 4, '2018-05-22 01:46:38', '2018-05-22 01:47:05');

-- --------------------------------------------------------

--
-- Table structure for table `listings_galleys`
--

CREATE TABLE `listings_galleys` (
  `id` int(10) UNSIGNED NOT NULL,
  `listing_id` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `listings_galleys`
--

INSERT INTO `listings_galleys` (`id`, `listing_id`, `image`, `created_at`, `updated_at`) VALUES
(1, 9, 'gallery/aAZt5PhkeEgrHGsZUJnkPgI7BngUJKpyFxkQL3WQ.png', '2018-05-16 09:02:46', '2018-05-16 09:02:46'),
(2, 9, 'gallery/nDYqPVTd8cHjuyqgEP30Nj0LimX9RuYEoxYZ6JzN.png', '2018-05-16 09:02:46', '2018-05-16 09:02:46'),
(3, 9, 'gallery/1YNe2uJlMY7lJeDVmJbfdZuSNCNW3HvtpBeKeuhU.jpeg', '2018-05-16 09:02:46', '2018-05-16 09:02:46'),
(4, 10, 'gallery/q0i0D2vw8pGJ8QQpPtw9ODEutKEAb2Nl2OIAPbfy.jpeg', '2018-05-20 12:58:47', '2018-05-20 12:58:47'),
(7, 11, 'gallery/Rm0DNKjj6IXeC7ZwECwlnpc4dFoG87gVrHMkiqvu.png', '2018-05-22 01:46:39', '2018-05-22 01:46:39'),
(8, 11, 'gallery/N0TQGbfzYP53pk4AWi32IA9YQYMlRdVqFwieXndq.jpeg', '2018-05-22 01:46:39', '2018-05-22 01:46:39'),
(9, 11, 'gallery/TjDCtT3GVYfCovzroBrY5m3MRWxeBUroCiHbJrmi.jpeg', '2018-05-22 01:46:39', '2018-05-22 01:46:39');

-- --------------------------------------------------------

--
-- Table structure for table `listing_reviews`
--

CREATE TABLE `listing_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `listingid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `review` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `listing_reviews`
--

INSERT INTO `listing_reviews` (`id`, `listingid`, `userid`, `review`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'listings first review by me', '2018-05-07 04:43:20', '2018-05-07 04:43:20'),
(2, 1, 2, 'second review', '2018-05-07 04:43:40', '2018-05-07 04:43:40'),
(3, 5, 2, 'Accer revies', '2018-05-07 04:54:25', '2018-05-07 04:54:25'),
(4, 5, 2, 'accer reviews 2', '2018-05-07 04:54:39', '2018-05-07 04:54:39'),
(5, 6, 2, 'head and shoulder reviews', '2018-05-07 04:55:02', '2018-05-07 04:55:02'),
(6, 6, 2, 'head and shoulder reviews', '2018-05-07 04:55:02', '2018-05-07 04:55:02'),
(7, 7, 2, 'hitachi review', '2018-05-07 05:07:15', '2018-05-07 05:07:15'),
(8, 7, 2, '1 review done now observe url', '2018-05-07 05:08:33', '2018-05-07 05:08:33'),
(9, 5, 1, 'Need one better laptop', '2018-05-09 04:41:55', '2018-05-09 04:41:55');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_12_18_101212_create_admins_table', 1),
(4, '2017_12_18_144514_add_field_to_user_table', 1),
(5, '2017_12_18_152636_create_categories_table', 1),
(6, '2017_12_18_160612_add_otp_field_to_user_table', 1),
(7, '2017_12_18_162540_add_messagestatus_field_to_user_table', 1),
(8, '2017_12_19_054536_create_listings_table', 1),
(9, '2017_12_19_073742_create_states_table', 1),
(10, '2017_12_19_114758_create_cities_table', 1),
(11, '2017_12_29_141204_create_addpostings_table', 1),
(12, '2018_01_27_054726_create_subcategories_table', 1),
(13, '2018_02_23_104211_create_listings_galleys_table', 1),
(14, '2018_05_07_090250_create_classified_reviews_table', 2),
(15, '2018_05_07_100234_create_listing_reviews_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('anaicut.rajeswari@gmail.com', '$2y$10$v.vC03W8bAzyYgLU7vOurekSmJymUvqf9W.5.rpANQWBPNvPYXQIa', '2018-05-20 08:14:03');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Andhra Pradesh', '2018-05-04 05:56:28', '2018-05-04 05:56:28');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `category_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 2, 'Second Hand', '2018-05-04 06:01:53', '2018-05-04 06:01:53'),
(2, 1, 'New', '2018-05-07 02:14:35', '2018-05-07 02:14:35'),
(3, 3, 'Materials', '2018-05-07 02:14:54', '2018-05-07 02:14:54'),
(4, 5, 'second Hand', '2018-05-07 02:15:25', '2018-05-07 02:15:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `otp` int(11) NOT NULL,
  `messageStatus` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `facebook_id`, `dob`, `address`, `image`, `referal`, `remember_token`, `created_at`, `updated_at`, `mobile`, `status`, `otp`, `messageStatus`) VALUES
(1, 'Rajesh', 'rajeshkevin.7@gmail.com', '$2y$10$6oiJa8s7Za2dGw1xdHHqluLL0ub7a7JBhtaBT7SvEN.DGAPvJB1e2', NULL, NULL, NULL, NULL, NULL, 'jkjkwFtLIyUNLmXUTmKHcEWqEXhs7AK0hpZNQBNWwh2GGxRb0vhoTey5yy62', '2018-05-04 05:46:41', '2018-05-16 07:30:41', '8056096993', 1, 329360, 1),
(2, 'mamatha', 'mamatha.vankayalapati@gmail.com', '$2y$10$uQ8lmVo1aSrJ9iPi/mNhUuZm7HpHCT2nrCy8uGNIdk3hU2F7MMIQW', NULL, NULL, NULL, NULL, NULL, 'pzb5Zq8yMOtzDIK0rM52HuMDKeEEPnzVDnFZbtkTSEuIUkhVKU89Q2jp2npl', '2018-05-07 02:41:21', '2018-05-07 02:41:43', '9133902535', 1, 728989, 1),
(3, 'Marco', 'Panasonik1@web.de', '$2y$10$Tp/STGj/HyIPqtn3cb9AgOP8bJfhf3Uu0tk8600c8wVC/Ski63wDi', NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-15 10:28:31', '2018-05-15 10:34:48', '0171837351', 0, 646655, 1),
(4, 'Marco', 'Marco_auto3@web.de', '$2y$10$Q4vse/CxFQ2e2RJtV7Lw0.D1X7D58PicLLMhGXsywyf5bP5eCkDkC', NULL, NULL, NULL, NULL, '2055184', NULL, '2018-05-15 10:42:13', '2018-05-15 10:58:27', '1718373513', 0, 560387, 1),
(5, 'Venky Vj', 'venkyvj@outlook.com', '$2y$10$Y4GfBmWOAUi7D74tUo7dAOMoexH1dgSeECicCJ4gAnjR7kILfBSua', '1395188760580550', NULL, NULL, NULL, NULL, 'rSHgtywQfvtASpUEYbjH2fkonFHCO7hePIxWuSc2DE1n2A2AfCY6jsPlfSUF', '2018-05-16 08:51:52', '2018-05-16 08:52:08', '7013089044', 1, 409701, 1),
(6, 'raji', 'anaicut.rajeswari@gmail.com', '$2y$10$yHrzBMSci9Jf7sdjiaZ8S.u/wHMXUHEJDxQKlgSiJ1X8sDhFJE2ZG', NULL, NULL, NULL, 'raji-6-1526804214.jpg', NULL, 'YDNhuYPDWyWpK0bQTq8VDWHmK59X2Y3H54jV6DaahVXPpJ0p8AyayHFxiGi4', '2018-05-20 08:10:08', '2018-05-20 08:16:54', '9347033337', 1, 798441, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addpostings`
--
ALTER TABLE `addpostings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classified_reviews`
--
ALTER TABLE `classified_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `listings`
--
ALTER TABLE `listings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `listings_galleys`
--
ALTER TABLE `listings_galleys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `listing_reviews`
--
ALTER TABLE `listing_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addpostings`
--
ALTER TABLE `addpostings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `classified_reviews`
--
ALTER TABLE `classified_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `listings`
--
ALTER TABLE `listings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `listings_galleys`
--
ALTER TABLE `listings_galleys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `listing_reviews`
--
ALTER TABLE `listing_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
