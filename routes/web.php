<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/updateProfilePhoto', 'profilePicController@update_photo_crop');

Route::get('/', function () {
    return view('frontend.home');
});
Route::get('/home', function () {
    return view('frontend.home');
});

Auth::routes();


Route::get('/password/otp','Auth\ForgotPasswordController@forgetPasswordView');
Route::post('/password/otp','Auth\ForgotPasswordController@forgetPasswordOtp')
->name('forgetPasswordOtp');
Route::post('/password/reset','Auth\ForgotPasswordController@forgetPasswordUpdate')
->name('forgetPasswordReset');
Route::get('/dashboard','HomeController@dashboard');

Route::prefix('luckylu')->group(function(){
  Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
  Route::post('/login','Auth\AdminLoginController@Login')->name('admin.login.submit');
  Route::get('/', 'AdminController@index')->name('admin.dashboard');
  Route::get('/logout', 'Auth\AdminLoginController@logout');
  Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

  Route::post('/updatelists','AdminReportController@updatelists')->name('updatelists');
  // Categories
  // BackEnd
  Route::resource('/categories','CategoryController');
  Route::resource('/subcategories','SubcategoriesController');
  Route::get('/subcategories/delete/{id}', 'SubcategoriesController@destroy');

  // Classifieds
  // BackEnd
  Route::get('/classifieds/edit/{id}','AdminReportController@editads');
  Route::post('/updateaddpost','AdminReportController@update')->name('updateaddpost');

  Route::resource('/states','StateController');
  Route::resource('/city','CityController');
  Route::get('/reports/listing/{id}','AdminReportController@listdestroy')->name('list.destroy');
  Route::get('/reports/view/{id}','AdminReportController@listshow');
  Route::get('/reports/listing','AdminReportController@lists');
  Route::get('/reports/users','AdminReportController@users');
  Route::get('/reports/classifieds','AdminReportController@ads');
  Route::get('/reports/classifieds/{id}','AdminReportController@adsdestroy');
  Route::get('/listings/edit/{id}','AdminReportController@editlists');
  Route::get('/delete/{id}/{img}','AdminReportController@deleteimage');
  Route::get('/gallerydelete/{id}','AdminReportController@gallerydelete');
  Route::get('/coverdelete/{id}','AdminReportController@coverdelete');

});

// Search
Route::get('/search/listing', 'SearchController@listingForward')->name('searchListing');
Route::get('/search/listing/{city}/{category}', 'SearchController@listing');
Route::get('/search/classifieds', 'SearchController@listingForwardClassifieds')->name('searchListingClassifieds');
Route::get('/search/classifieds/{city}/{category}', 'SearchController@classifieds');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user/logout', 'Auth\LoginController@UserLogout');
Route::post('/user/logout', 'Auth\LoginController@UserLogout')->name('user.logout');


Route::get('/cityAll','CityController@cityAll');
Route::get('/categoriesAll','CategoryController@categoriesAll');
Route::get('/cityAllData','CityController@cityAllData');

// CitySelect
Route::post('/selectCity', 'CityController@selectCity')->name('selectCity');


Route::get('verifymobile/{mobile}', 'MobileVerificationController@index');
Route::post('verifymobile', 'MobileVerificationController@resend')->name('mobile.resend');
Route::post('verify', 'MobileVerificationController@store')->name('mobile.verify');


Route::resource('addposting','AddpostingController');
// Route::get('/addposting/add', 'AddpostingController@add');
// classifieds
// FrontEnd

Route::get('/classifieds', 'AddpostingController@allads');
Route::get('/classifieds/{id}', 'AddpostingController@showads');
Route::post('classifiedreviews','ClassifiedReviewsController@store');
Route::post('/SubCategoryFind', 'AddpostingController@SubCategoryFind');


// Category
// FrontEnd
Route::resource('categories','CategoryController');
Route::get('/categorie/ajax/{id}','CategoryController@getSubCategories');

Route::get('profile/{id}/edit','ProfileController@edit');
Route::post('profile/update','ProfileController@update')->name('profileUpdate');
Route::get('update/{id}/edit','ProfileController@passwordedit');
Route::post('/updatepassword','ProfileController@updatepassword')->name('updatepassword');
Route::resource('intel','ProfileController');
Route::get('/alllists', 'HomeController@alllistings');


Route::get('verifymobileemail/{email}', 'MobileVerificationController@verifymobileemail');



// Facebook Socialite
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('facebookregister', 'Auth\SociliteController@savefacebookform');
Route::post('facebookregister', 'Auth\SociliteController@savefacebook')->name('facebook.register');

// Google Socialite
Route::get('/login/google', 'Auth\LoginController@redirectToProviderGoogle');
Route::get('/login/google/callback', 'Auth\LoginController@handleProviderCallbackGoogle');
Route::get('googleregister', 'Auth\SociliteController@savegoogleform');
Route::post('googleregister', 'Auth\SociliteController@savegoogle')->name('google.register');


// Listing
// FrontEnd
Route::resource('listing','ListingController');
Route::get('list/{id}','HomeController@show');
Route::get('result/{id}','HomeController@result');
Route::get('{city}/{slug}','HomeController@slug')->name('slugListing');
Route::get('/success','ListingController@success');
Route::get('/listing/{id}/edit','ListingController@edit');
Route::post('/listing/{id}','ListingController@update');
Route::post('listingreviews','ListingReviewsController@store');
