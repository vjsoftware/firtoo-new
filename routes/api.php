<?php

use Illuminate\Http\Request;
use App\Listing;
use App\ListingsGalley;
use App\Addposting;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/deleteListingGallery', function (Request $request) {
    $id = $request['id'];
    // return $id;
    $image = ListingsGalley::find($id);
    $split = explode('/', $image['image']);
    $path = $split[4].'/'.$split[5];
    // return $path;
    if(Storage::disk('s3')->exists($path)) {
      Storage::disk('s3')->delete($path);
      $removeImage = DB::delete("delete from `listings_galleys` WHERE `id` = '$id'");
      if ($removeImage) {
        return 'D';
      } else {
        return 'F';
      }
    }
    return 'F';
});
Route::post('/deleteListingCover', function (Request $request) {
    $id = $request['id'];
    // return $id;
    $image = Listing::find($id);
    $split = explode('/', $image['cover']);
    $path = $split[4].'/'.$split[5];
    // return $path;
    if(Storage::disk('s3')->exists($path)) {
      Storage::disk('s3')->delete($path);
      $removeImage = DB::delete("update listings set `cover` = '' where `id` = '$id'");
      if ($removeImage) {
        return 'D';
      } else {
        return 'F';
      }
    }
    return 'F';
});
Route::post('/deleteClassifyImgOne', function (Request $request) {
    $id = $request['id'];
    // return $id;
    $image = Addposting::find($id);
    $split = explode('/', $image['image']);
    $path = $split[4].'/'.$split[5];
    // return $path;
    if(Storage::disk('s3')->exists($path)) {
      Storage::disk('s3')->delete($path);
      $removeImage = DB::delete("update addpostings set `image` = '' where `id` = '$id'");
      if ($removeImage) {
        return 'D';
      } else {
        return 'F';
      }
    }
    return 'F';
});
Route::post('/deleteClassifyImgTwo', function (Request $request) {
    $id = $request['id'];
    // return $id;
    $image = Addposting::find($id);
    $split = explode('/', $image['image2']);
    $path = $split[4].'/'.$split[5];
    // return $path;
    if(Storage::disk('s3')->exists($path)) {
      Storage::disk('s3')->delete($path);
      $removeImage = DB::delete("update addpostings set `image2` = '' where `id` = '$id'");
      if ($removeImage) {
        return 'D';
      } else {
        return 'F';
      }
    }
    return 'F';
});
Route::post('/deleteClassifyImgThree', function (Request $request) {
    $id = $request['id'];
    // return $id;
    $image = Addposting::find($id);
    $split = explode('/', $image['image3']);
    $path = $split[4].'/'.$split[5];
    // return $path;
    if(Storage::disk('s3')->exists($path)) {
      Storage::disk('s3')->delete($path);
      $removeImage = DB::delete("update addpostings set `image3` = '' where `id` = '$id'");
      if ($removeImage) {
        return 'D';
      } else {
        return 'F';
      }
    }
    return 'F';
});
