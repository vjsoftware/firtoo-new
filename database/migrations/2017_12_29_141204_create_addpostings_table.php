<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddpostingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addpostings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('category_id');
            $table->integer('subcategory_id');
            $table->string('city');
            $table->string('item');
            $table->string('price');
            $table->string('mobile');
            $table->string('description')->nullable();
            $table->integer('status');
            $table->string('bought_year')->nullable();
            $table->string('image')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addpostings');
    }
}
